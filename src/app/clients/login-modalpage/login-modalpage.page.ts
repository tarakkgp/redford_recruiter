import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ModalController,NavParams} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {DataserviceService} from '../../Service/dataservice.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login-modalpage',
  templateUrl: './login-modalpage.page.html',
  styleUrls: ['./login-modalpage.page.scss'],
})
export class LoginModalpagePage implements OnInit {
  phone:any;
  ccode:any;
  cname:any;
  csize:any;
  ctype:any;
  size:any;
  type:any;
  companytypelist:any;
  companysizelist:any;
  client_id:any;

  constructor(public nav: NavParams,
    public modalCtrl: ModalController,
    public storage: Storage,
    public service: DataserviceService,
    public router: Router) {
      this.client_id = this.nav.get('data').client_id;
     }

  ngOnInit() {
    this.service.clientcompany_size().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]=="true"){
        this.companysizelist=resArray[2];
      }
    });
    
    this.service.get_method_apicall('client-registration-form').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]==true){
        this.companytypelist= resArray[2];
        console.log(this.companytypelist);
      }
      //this.service.hideLoader();
    });
  }

  async validateuser(form: NgForm){   
    console.log(form);
    if (form.invalid || this. csize=='Company Size' || this.ctype == 'Company Type') {
        //alert('invalid');
      return;
    }
   let postdata = {
    "data":{
    "client_id": this.client_id,
    "mob_country_code": this.ccode,
    "mobile": this.phone,
    "company_name": this.cname,
    "company_size": this.csize,
    "company_type": this.ctype
      } 
    }; 
      console.log(postdata);
      this.service.alllogin_registration(postdata,"provider-client-update").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          this.modalCtrl.dismiss();
          this.storage.set("login_credential",result).then(()=>{
            this.service.presentToast(resArray[1]);
            this.service.get_login_credential();
            this.router.navigateByUrl('/client/profile');
          });
        }else{
          
        }
      });
  }

  closeModal(){
    this.modalCtrl.dismiss();
  }

}
