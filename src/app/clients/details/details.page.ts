import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  comid;
  jobtitle;
  jobcat;
  jobdes;
  jobres;
  jobskill;
  jobqfctn;
  jobreq;
  jobcity;
  jobste;
  jobcunty;
  jobstatus;
  jobexp;
  jobsal;
  jobcurrency;
  statustitle;

  comName;
  comCat;
  comlogo;
  comSize;
  comMail;
  comSite;
  healthLicence;
  licenceNo;

  constructor(
    public service: DataserviceService,
    public acRoute: ActivatedRoute,
    public route: Router
  ) { }

  ngOnInit() {
    let postdata = {
      "data":{
        "job_id": this.acRoute.snapshot.paramMap.get('id')
      }
    }
    //console.log(postdata)
    this.service.post_request_api_call(postdata,'client-job-details').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.comid = resArray[2].company_id;
        this.jobtitle = resArray[2].job_title;
        this.jobcat = resArray[2].job_category_title;
        this.jobdes = resArray[2].job_description;
        this.jobres = resArray[2].responsibilities;
        this.jobskill = resArray[2].skill_set;
        this.jobqfctn = resArray[2].min_qualification;
        this.jobreq = resArray[2].requirements;
        this.jobcity = resArray[2].basic_info_city;
        this.jobste = resArray[2].basic_info_state;
        this.jobcunty = resArray[2].basic_info_cuntry;
        this.jobstatus = resArray[2].job_opening_status;
        this.jobexp = resArray[2].person_work_exp;
        this.jobsal = resArray[2].person_salary;
        this.jobcurrency = resArray[2].currency;
        this.statustitle = resArray[2].job_opening_status_title;
        this.healthLicence = resArray[2].healthcare_license;
        this.licenceNo = resArray[2].licenses_number;

        this.comName = resArray[2].company_name;
        this.comCat = resArray[2].company_category_title;
        this.comlogo = resArray[2].company_logo;
        this.comSize = resArray[2].company_size;
        this.comMail = resArray[2].company_email;
        this.comSite = resArray[2].company_website;
      }
    });
  }

  comJobList(id){
    if(this.service.login_type != 'admin'){
      this.route.navigate(['/client/job-listing', {id:id}]);
    }else{
      this.route.navigate(['/admin/job-listing', {id:id, cid:this.acRoute.snapshot.paramMap.get('cid')}]);
    }
  }

  goCompany(){
    this.route.navigate(['/client/company-details', {cid:this.comid}]);
  }

}
