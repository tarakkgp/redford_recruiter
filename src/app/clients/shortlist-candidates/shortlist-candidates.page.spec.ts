import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortlistCandidatesPage } from './shortlist-candidates.page';

describe('ShortlistCandidatesPage', () => {
  let component: ShortlistCandidatesPage;
  let fixture: ComponentFixture<ShortlistCandidatesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortlistCandidatesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortlistCandidatesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
