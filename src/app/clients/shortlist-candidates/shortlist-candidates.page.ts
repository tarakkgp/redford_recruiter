import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-shortlist-candidates',
  templateUrl: './shortlist-candidates.page.html',
  styleUrls: ['./shortlist-candidates.page.scss'],
})
export class ShortlistCandidatesPage implements OnInit {
  jobid:any;
  candidatelist=[];
  jobdetails: any;
  allcandidatelist: any;

  constructor(public router :Router,
    public service :DataserviceService,
    public acroute :ActivatedRoute,
    public storage :Storage,
    public alertController :AlertController) { }

  ngOnInit() {
    this.jobid = this.acroute.snapshot.paramMap.get('jobid');
    this.shortlistCandidateListapi();
  }

  shortlistCandidateListapi(){
    let postdata = {
      "data":{
        "job_id": this.jobid
      }
    }
    console.log(postdata)
    this.service.post_request_api_call(postdata,'shortlisted-candidate-list').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.jobdetails = resArray[2];
        this.allcandidatelist =resArray[3];
        console.log(this.allcandidatelist);
      }
    });
  }

  manageCandidate(){
    if(this.candidatelist.length>0){
      this.storage.get('login_credential').then((val)=>{
        let postdata = {
          "data":{
            "job_id": this.jobid,
            "user_id": this.service.userid,
            "user_type": this.service.login_type,
            "candidate_id": this.candidatelist
          }
        }
        console.log(postdata)
        this.service.post_request_api_call(postdata,'assigned-candidate-upload').then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.service.Successalert(resArray[2]);
            if(this.service.login_type != 'admin'){
              this.router.navigate(['/client/shortlisted-candidates', {jobid:this.jobid}]);
            }else{
              this.router.navigate(['/admin/shortlisted-candidates', {jobid:this.jobid}]);
            }
          }else{
            this.service.Validalert(resArray[2]);
          }
        });
      });
    }else{
      this.service.Validalert("Please select atleast one candidate");
    }  
  }

  checking(ev){
    if(ev.target.checked){
      //console.log(ev.target.value)
      this.candidatelist.push(ev.target.value);
      console.log(this.candidatelist);
    }else{
      let indexNo = this.candidatelist.indexOf(ev.target.value);
      console.log(indexNo);
      this.candidatelist.splice(indexNo, 1);
      console.log(this.candidatelist);
    }
  }

  canDetails(id){
    if(this.service.login_type == "admin"){
      window.open('/admin/candidate-details;can_id='+id, '_blank')
    }else{
      window.open('/client/candidate-details;can_id='+id, '_blank')
    }
    // this.router.navigate(['/client/candidate-details', {can_id:id}]);
  }

}
