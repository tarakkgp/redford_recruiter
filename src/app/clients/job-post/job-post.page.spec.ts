import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPostPage } from './job-post.page';

describe('JobPostPage', () => {
  let component: JobPostPage;
  let fixture: ComponentFixture<JobPostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPostPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
