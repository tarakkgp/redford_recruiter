import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.page.html',
  styleUrls: ['./company-details.page.scss'],
})
export class CompanyDetailsPage implements OnInit {

  details;
  clientID;
  redirect;

  constructor(
    public router: Router,
    public acRoute: ActivatedRoute,
    public service: DataserviceService,
    public alertController: AlertController
  ) { }

  ngOnInit() {
    this.redirect = this.acRoute.snapshot.paramMap.get('redirect');
    // this.details = this.acRoute.snapshot.paramMap.get('cd');
    // console.log(this.details);
    /* this.acRoute.queryParams.subscribe(res=>{
      this.details = JSON.parse(res.cd);
      console.log(this.details)
    }) */
  }

  ionViewDidEnter(){
    //console.log(this.service.login_type);
    let postData = {
      "data":{
        "company_id": this.acRoute.snapshot.paramMap.get('cid')
      }
     }
    this.service.post_request_api_call(postData, 'client-company-details').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.details = resArray[2];
        this.clientID = resArray[2][0].client_id;
        // console.log(this.details.company_name);
      }
    })
  }

  addCompany(){
    if(this.service.login_type == 'admin'){
      this.router.navigate(['/admin/add-company', {cID:this.clientID}]);
    }else{
      this.router.navigateByUrl('/client/company');
    }
  }

  editComDetails(d){
    if(this.service.login_type == 'admin'){
      this.router.navigate(['/admin/edit-company', {id:d, cID:this.clientID}]);
    }else{
      this.router.navigate(['/client/company', {id:d}]);
    }
  }

  deleteCompany(d){
    let postData = {
      "data":{
        "company_id": d
      }
     }
    this.service.post_request_api_call(postData, 'client-company-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        //this.details = resArray[2];
        //this.service.presentToast(resArray[2]);
        if(this.service.login_type == 'admin'){
          this.service.showalert(resArray[2], this.redirect);
        }else{
          this.router.navigateByUrl('/client/company-list');
          // console.log(this.details.company_name);
        }
      }
    })
  }

  async presentAlertConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deleteCompany(id);
          }
        }
      ]
    });

    await alert.present();
  }

  addJob(id){
    if(this.service.login_type == 'admin'){
      this.router.navigate(['/admin/job-post', {id:id,cid:this.clientID}]);
    }else{
      this.router.navigate(['/client/job-post', {id:id}]);
    }
  }

  comJobList(id){
    if(this.service.login_type == 'admin'){
      this.router.navigate(['/admin/job-listing', {id:id,cid:this.clientID}]);
    }else{
      this.router.navigate(['/client/job-listing', {id:id}]);
    }
  }

}
