import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {DataserviceService} from '../../Service/dataservice.service';
import {AlertController} from '@ionic/angular';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-clients-dashboard',
  templateUrl: './clients-dashboard.page.html',
  styleUrls: ['./clients-dashboard.page.scss'],
})
export class ClientsDashboardPage implements OnInit {
  client_id:any;
  job_list:any;
  jobstatus:boolean=false;
  fff;
  totCom;
  totJob;
  totCan;

  constructor(private storage: Storage,
    private service: DataserviceService,
    private alertController:AlertController,
    private router: Router) { }

  ngOnInit() {
        //get all details from storage..
        this.storage.get("login_credential").then((val)=>{
          if(val){
            const resArray = Object.keys(val).map(i => val[i]);
            //console.log(resArray);
            if(resArray[0]=="true"){
              //console.log(val);
              this.client_id=resArray[2].client_id;
              console.log(this.client_id);
    
              //candidate dashboard api calling..
              // let postdata = {
              //   "data":{"client_id":this.client_id
              //   }
              // };
              // console.log(postdata);
              // this.service.post_request_api_call(postdata,"client-job-listing").then((result)=>{
              //   const resArray = Object.keys(result).map(i => result[i]);
              //   console.log(resArray);
              //   if(resArray[1]){ 

              //     this.job_list=resArray[2];
              //     this.jobstatus=true;
              //     console.log(this.job_list);
              //   }else{
              //     //this.service.presentToast(resArray[0]);
              //   }
              //   //this.service.hideLoader();
              // });
              let data = {
                "data":{
                  "client_id":this.client_id
                }
              };
              this.service.post_request_api_call(data,"client-dashboard").then((result)=>{
                //const resArray = Object.keys(result).map(i => result[i]);
                this.fff = result;
                console.log(result);
                if(this.fff.success){
                  this.totCom = this.fff.client_company_count;
                  this.totJob = this.fff.client_job_count;
                  this.totCan = this.fff.candidate_count;
                }
              });
            }
          }else{
            this.router.navigateByUrl('/home');
          }  
        });
    
  }

  clientCompanyList(){
    this.router.navigateByUrl('/client/company-list');
  }

  clientJobPost(){
    this.router.navigateByUrl('/client/job-post');
  }

  clientViewProfile(){
    this.router.navigateByUrl('/client/profile');
  }

  clientEditProfile(){
    this.router.navigate(['/client/profile', {edit:true}]);
  }

}
