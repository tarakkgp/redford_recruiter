import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController,ModalController} from '@ionic/angular';
import {EditShortlistedCandidatePage} from '../edit-shortlisted-candidate/edit-shortlisted-candidate.page';

@Component({
  selector: 'app-shortlisted-candidates',
  templateUrl: './shortlisted-candidates.page.html',
  styleUrls: ['./shortlisted-candidates.page.scss'],
})
export class ShortlistedCandidatesPage implements OnInit {
  jobid:any;
  jobdetails:any;
  candidatelist:any;
  count:any;
  statusHistory;
  
  constructor(
    public router: Router,
    public service: DataserviceService,
    public acroute: ActivatedRoute,
    public storage: Storage,
    public alertController: AlertController,
    public modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  ionViewWillEnter(){
    this.jobid = this.acroute.snapshot.paramMap.get('jobid');
    console.log(this.jobid);
    this.shortlistedCandidateListapi();
  }

  shortlistedCandidateListapi(){
    let postdata = {
      "data":{
        "job_id": this.jobid
      }
    }
    console.log(postdata)
    this.service.post_request_api_call(postdata,'assigned-candidate-list').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.jobdetails = resArray[2];
        this.candidatelist =resArray[3];
        this.count=this.candidatelist.length;
      }
    });
  }

  gotoJobassigned(){
    if(this.service.login_type != 'admin'){
      this.router.navigate(['/client/shortlist-candidates', {jobid:this.jobid}]);
    }else{
      this.router.navigate(['/admin/shortlist-candidates', {jobid:this.jobid}]);
    }
  }

  deleteCandidate(d){
    let postData = {
      "data":{
        "assigned_job_to_candidates_id": d
      }
     }
    this.service.post_request_api_call(postData, 'assigned-candidate-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.shortlistedCandidateListapi();
      }
    })
  }

  async DeleteConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deleteCandidate(id);
          }
        }
      ]
    });

    await alert.present();
  }

  

  async editassignedCandidate(candidate_id){
    const modal = await this.modalCtrl.create({
      component: EditShortlistedCandidatePage,
      componentProps:{data:candidate_id},
      cssClass : 'candidate_modal3',
      showBackdrop: true,
      backdropDismiss:false
    });
    modal.onDidDismiss().then(details => {
    if(details.data){
      this.shortlistedCandidateListapi();
    }
  });
  return await modal.present();
  }

  statusInfo(id){
    //console.log(id);
    let postdata = {
      "data":{
        "job_id": this.jobid,
        "candidate_id": id
      }
    };
    console.log(postdata)
    this.service.post_request_api_call(postdata,'candidate-job-application-status-history').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.statusHistory = resArray[2];
        // this.service.presentToast(resArray[1]);
        // window.location.reload();
      }
    });
  }

  canDetails(id){
    if(this.service.login_type == "admin"){
      window.open('/admin/candidate-details;can_id='+id, '_blank')
    }else{
      window.open('/client/candidate-details;can_id='+id, '_blank')
    }
    // this.router.navigate(['/client/candidate-details', {can_id:id}]);
  }

}
