import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditShortlistedCandidatePage } from './edit-shortlisted-candidate.page';

describe('EditShortlistedCandidatePage', () => {
  let component: EditShortlistedCandidatePage;
  let fixture: ComponentFixture<EditShortlistedCandidatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditShortlistedCandidatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditShortlistedCandidatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
