import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import {ModalController,NavParams} from '@ionic/angular';
import {NgForm} from '@angular/forms';
import { IfStmt } from '@angular/compiler';


@Component({
  selector: 'app-edit-shortlisted-candidate',
  templateUrl: './edit-shortlisted-candidate.page.html',
  styleUrls: ['./edit-shortlisted-candidate.page.scss'],
})
export class EditShortlistedCandidatePage implements OnInit {
  substatus:any;
  mainstatus:any;
  main:any;
  sub:any;
  mainstatuslist:any;
  substatuslist:any;
  assignedid: any;

  constructor(public service :DataserviceService,public mdctrl:ModalController,
    public nav: NavParams) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.assignedid= this.nav.get('data');
    console.log(this.assignedid);
    this.getmainstatuslist();
    this.get_details();
  }

  getmainstatuslist(){
    this.service.post_request_api_call("", 'candidate-job-opening-status').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.mainstatuslist=resArray[2];
      }
    })
  }

  get_Substatuslist(d){
    console.log(d);
    this.sub="Select Substatus";
    this.substatus="Select Substatus";
    let postData = {
      "data":{
        "candidate_job_opening_status_id": d
      }
     }
    this.service.post_request_api_call(postData, 'candidate-job-opening-sub-status').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.substatuslist=resArray[2];
      }
    });
  }

  get_details(){
    let postData = {
      "data":{
        "assigned_job_to_candidates_id": this.assignedid
      }
     }
    this.service.post_request_api_call(postData, 'assigned-candidate-details').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        if(resArray[2].candidate_job_opening_status_id){
          this.mainstatus=resArray[2].candidate_job_opening_status_id;
          this.get_Substatuslist(resArray[2].candidate_job_opening_status_id);
          if(resArray[2].candidate_job_opening_sub_status_id){
            this.substatus = resArray[2].candidate_job_opening_sub_status_id;
          }else{
            this.sub="Select Substatus";
            this.substatus="Select Substatus";
          }
          
        }else{
          this.main="Select Status";
          this.mainstatus="Select Status";
          this.sub="Select Substatus";
          this.substatus="Select Substatus";
        }
        //this.substatuslist=resArray[2];
      }
    });
  }

  async validateform(form: NgForm){
    console.log(form);
    if (form.invalid || this.substatus == 'Select Substatus' || this.mainstatus == 'Select Status') {
        //alert('invalid');
      return;
    }
    let postData = {
      "data":{
        "user_id":this.service.userid,
        "user_type":this.service.login_type,
        "candidate_job_opening_status_id": this.mainstatus,
        "candidate_job_opening_sub_status_id": this.substatus,
        "assigned_job_to_candidates_id": this.assignedid
      }
    }
    console.log(postData);
    this.service.post_request_api_call(postData, 'assigned-candidate-update').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.mdctrl.dismiss(true);
        this.service.Successalert(resArray[2]);
      }
    }); 
  }

  closeModal(){
    this.mdctrl.dismiss();
  }

}
