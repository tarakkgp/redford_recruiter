import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-company',
  templateUrl: './company.page.html',
  styleUrls: ['./company.page.scss'],
})
export class CompanyPage implements OnInit {

  comName:any;
  csize:any;
  ctype:any;
  comWeb:any;
  description:any;
  comCountry:any;
  comState:any;
  comCity:any;
  comCntPrsn:any;
  comCntMail:any;
  comCntPh:any;
  comAddress:any;
  comID:any;
  conInfoId:any;
  cmpphone:any;
  cmpcode:any;
  cmpemail:any;
  concode:any;
  size:any;
  type:any;
  country:any;
  state:any;
  companysizelist:any;
  companytypelist:any;
  countrylist:any;
  statelist:any;
  rdrRslt:any;
  extension:any;
  editedfilename:any ='';
  addoredit;
  companyUpdate;

  constructor(
    public service: DataserviceService,
    public acRoute: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.service.getCountry().then((res)=>{
      console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.countrylist=resArray[0];
      console.log(resArray);
    });
  }

  ionViewDidEnter(){

    this.service.clientcompany_size().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]=="true"){
        this.companysizelist=resArray[2];
      }
    });
    
    this.service.get_method_apicall('client-registration-form').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]==true){
        this.companytypelist= resArray[2];
        console.log(this.companytypelist);
      }
      //this.service.hideLoader();
    });
    
    if(this.acRoute.snapshot.paramMap.get('id')){
      this.addoredit = "Edit";
      this.companyUpdate = "UPDATE";
      let postData = {
        "data":{
          "company_id": this.acRoute.snapshot.paramMap.get('id')
        }
       }
      this.service.post_request_api_call(postData, 'client-company-details').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.comName = resArray[2][0].company_name;
          this.csize = resArray[2][0].company_size;
          this.ctype = resArray[2][0].category;
          this.comWeb = resArray[2][0].website;
          this.description =resArray[2][0].company_description;
          if(resArray[2][0].country){
            this.comCountry = resArray[2][0].country;
            this.onChange(resArray[2][0].country);
          }
          if(resArray[2][0].state){
            this.comState = resArray[2][0].state;
          }
          this.comCity = resArray[2][0].city;
          this.comCntPrsn = resArray[2][0].contact_person_name;
          this.comCntMail = resArray[2][0].contact_person_email;
          this.comCntPh = resArray[2][0].contact_person_mobile;
          this.comAddress = resArray[2][0].address;
          this.comID = resArray[2][0].company_id;
          this.conInfoId = resArray[2][0].company_contact_info_id;
          this.concode = resArray[2][0].contact_person_country_code;
          this.cmpemail = resArray[2][0].company_email;
          this.cmpphone = resArray[2][0].company_phone;
          this.cmpcode = resArray[2][0].company_country_code;
          this.rdrRslt = resArray[2][0].logo;
          this.editedfilename = resArray[2][0].logo;
        }
      })
    }else{
      this.addoredit = "Add";
      this.companyUpdate = "SAVE";
    }
  }

  submitComData(register:NgForm){
    console.log(this.service.userid)
    if(this.editedfilename == this.rdrRslt){
      this.rdrRslt ='';
    }
      let postData = {
        "data":{
          "client_id": this.acRoute.snapshot.paramMap.get('cID')?this.acRoute.snapshot.paramMap.get('cID'):this.service.userid,
          "company_id": this.comID?this.comID:'',
          "company_contact_info_id": this.conInfoId?this.conInfoId:'',
          "company_name": this.comName,
          "company_size": this.csize,
          "company_email": this.cmpemail?this.cmpemail:'',
          "company_phone": this.cmpphone?this.cmpphone:'',
          "company_country_code": this.cmpcode,
          "company_description": this.description,
          "category": this.ctype,
          "address": this.comAddress,
          "city": this.comCity,
          "state": this.comState,
          "country": this.comCountry,
          "website": this.comWeb,
          "contact_person_name": this.comCntPrsn,
          "contact_person_email": this.comCntMail,
          "contact_person_mobile": this.comCntPh,
          "contact_person_country_code": this.concode,
          "company_logo": this.rdrRslt?this.rdrRslt:'',
          "file_extension": this.extension
        }
      }
      console.log(postData);
      this.service.post_request_api_call(postData, 'client-company-submit').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          //this.service.presentToast(resArray[2]);
          if(this.service.login_type == 'admin'){
            this.service.showalert(resArray[2], 'admin/company-list;c_id='+this.acRoute.snapshot.paramMap.get('cID'))
          }else{
            this.service.showalert(resArray[2], '/client/company-list');
          }
          //this.router.navigateByUrl('/client/company-list');
          //register.reset();
        }else{
          this.service.showalert("Error Occured", '')
        }
      })
  }

  validateuser(register:NgForm){
    console.log(register);
    if (register.invalid) {
        //alert('invalid');
      return;
    }
    this.submitComData(register);
  }

  onChange(e){
    console.log(e);
    this.state = 'Select State';
    this.comState = 'Select State';
    this.countrylist.forEach(element => {
      if(element.country==e){
        this.statelist = element.states;
        console.log(this.statelist)
      }
    });
  }

  changeImg(ev) {
    let file = ev.files[0];
    let file_name=file.name;
    this.extension=file_name.split('.').pop();
    if(file.type=="image/jpeg" || file.type=="image/png"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result)
        this.rdrRslt = reader.result;
        console.log(this.rdrRslt)
      }
      reader.readAsDataURL(file);
      console.log(this.rdrRslt);
    }else{
      this.service.Validalert("Only .jpg and .png file type are allowed.");
      ev.value='';
    }
    // let file_name=file.name;
    // this.extension=file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
  }

  uploadIMG(){
    //console.log("click");
    document.getElementById('videoInput').click();
  }

}
