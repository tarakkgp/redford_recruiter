import { Component, OnInit, ViewChild } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.page.html',
  styleUrls: ['./listing.page.scss'],
})
export class ListingPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  jobsListing = [];
  companyDetails:any;
  errmsg:any="";
  clientId;
  statusHistory;
  start:number = 0;
  limit:number = 10;
  totaldata;

  constructor(
    public service: DataserviceService,
    public acRoute: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.clientId = this.acRoute.snapshot.paramMap.get('cid');
  }

  ionViewDidEnter(){
    this.firstcallapi();
  }

  ionViewDidLeave(){
    this.jobsListing = [];
    //console.log('kkkkkk')
  }

  firstcallapi(){
    this.storage.get('login_credential').then((val)=>{
      if(val){
      let postdata = {
        "data":{
          "client_id": this.clientId?this.clientId:this.service.userid,
          "company_id": this.acRoute.snapshot.paramMap.get('id'),
          "start": this.start,
          "limit": this.limit
        }
      }
      console.log(postdata)
      this.service.post_request_api_call(postdata,'client-job-listing').then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          resArray[2].forEach(element => {
            this.jobsListing.push(element);
          });
          this.virtualScroll.checkEnd();
          this.totaldata = resArray[4];
          this.companyDetails= resArray[3];
          //console.log(this.companyDetails);
          this.errmsg="";
        }else{
          this.errmsg= resArray[2];
        }
      });
    }
    });
  }

  loadData(event) {
    this.start = this.start+this.limit;
    setTimeout(() => {
    
      this.firstcallapi()
      //Hide Infinite List Loader on Complete
      event.target.complete();

      //Rerender Virtual Scroll List After Adding New Data

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.start+this.limit >= this.totaldata) {
        event.target.disabled = true;
      }
    }, 500);
  }

  jobDetails(id){
    if(this.clientId){
      this.router.navigate(['/admin/job-details', {id:id, cid:this.clientId}]);
    }else{
      this.router.navigate(['/client/job-details', {id:id}]);
    }
  }

  addToFav(id){
    this.storage.get('login_credential').then((val)=>{
      if(val){
      let postdata = {
        "data":{
          "client_id": this.service.userid,
          "job_id": id
        }
      }
      console.log(postdata)
      this.service.post_request_api_call(postdata,'client-favourite-job-upload').then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.service.presentToast(resArray[2]);
        }
      });
    }
    });
  }

  addNewJob(){
    if(this.clientId){
      this.router.navigate(['/admin/job-post', {id:this.acRoute.snapshot.paramMap.get('id'),cid:this.clientId}]);
    }else{
      this.router.navigate(['/client/job-post', {id:this.acRoute.snapshot.paramMap.get('id')}]);
    }
  }

  addJob(cid,jid){
    console.log(cid,jid);
    if(this.clientId){
      this.router.navigate(['/admin/job-post', {id:cid,jobid:jid,cid:this.clientId}]);
    }else{
      this.router.navigate(['/client/job-post', {id:cid,jobid:jid}]);
    }
  }

  deleteJobapicall(id){
    let postdata = {
      "data":{
        "job_id": id
      }
    };
    console.log(postdata)
    this.service.post_request_api_call(postdata,'job-delete').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){
        this.service.presentToast(resArray[1]);
        window.location.reload();
      }
    });
  }

  async presentAlertConfirm_deletejob(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deleteJobapicall(id);
          }
        }
      ]
    });

    await alert.present();
  }

  shortlistedCandidate(jid){
    if(this.clientId){
      this.router.navigate(['/admin/shortlisted-candidates', {jobid:jid}]);
    }else{
      this.router.navigate(['/client/shortlisted-candidates', {jobid:jid}]);
    }
  }

  statusInfo(id){
    //console.log(id);
    let postdata = {
      "data":{
        "job_id": id
      }
    };
    console.log(postdata)
    this.service.post_request_api_call(postdata,'job-status-changed-history').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.statusHistory = resArray[2];
        // this.service.presentToast(resArray[1]);
        // window.location.reload();
      }
    });
  }

}
