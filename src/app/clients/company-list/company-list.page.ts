import { Component, OnInit, ViewChild } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.page.html',
  styleUrls: ['./company-list.page.scss'],
})
export class CompanyListPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  comDetls = [];
  clientID;
  admin;
  pages = []; ///pagination
  comDetlsShow = []; ///pagination
  start:number = 0;
  limit:number = 10;
  totaldata;

  constructor(
    public service: DataserviceService,
    public router: Router,
    public acRoute: ActivatedRoute
  ) { }

  ngOnInit(){
    this.clientID = this.acRoute.snapshot.paramMap.get('c_id');
    this.admin = this.acRoute.snapshot.paramMap.get('admin');
  }

  ionViewWillEnter(){
    this.newFunction();
  }

  ionViewDidLeave(){
    this.comDetls = [];
    this.start = 0;
    this.infiniteScroll.disabled = false;
  }

  newFunction(){
    if(!this.admin){
      let postData = {
        "data":{
          "client_id": this.clientID?this.clientID:this.service.userid,
          "start":this.start,
          "limit":this.limit
        }
      }
      console.log(postData);
      this.service.post_request_api_call(postData, 'client-company-listing').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        if(resArray[1]){
          resArray[2].forEach(element => {
            this.comDetls.push(element);
          });
          this.virtualScroll.checkEnd();
          this.totaldata = resArray[3];
          //this.calculatePage(resArray[2].length); ///pagination
        }else{
          this.comDetls = [];
        }
      })
    }else{
      let postData = {
        "data":{
          "user_type": 'admin',
          "start":this.start,
          "limit":this.limit
        }
      }
      console.log(postData);
      this.service.post_request_api_call(postData, 'client-company-listing').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        //console.log(resArray);
        if(resArray[1]){
          resArray[2].forEach(element => {
            this.comDetls.push(element);
          });
          this.virtualScroll.checkEnd();
          this.totaldata = resArray[3];
          if (this.start+this.limit >= this.totaldata) {
            this.infiniteScroll.disabled = true;
          }
          //this.calculatePage(resArray[2].length); ///pagination
        }else{
          this.comDetls = [];
        }
      })
    }
    
  }

  loadData(event) {
    this.start = this.start+this.limit;
    setTimeout(() => {
    
      this.newFunction();
      //Hide Infinite List Loader on Complete
      event.target.complete();

      //Rerender Virtual Scroll List After Adding New Data

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.start+this.limit >= this.totaldata) {
        event.target.disabled = true;
      }
    }, 500);
  }

///pagination
  calculatePage(totalData){
    this.pages = [];
    let totalPages = Math.floor(totalData/10)+1;
    for(let i = 0; i < totalPages; i++){
      this.pages.push(i+1);
    }
    this.showPagination(1);
  }

  showPagination(n){
    this.comDetlsShow = [];
    document.getElementById('showingResult').innerHTML = "";
    let p = (Number(n)-1)*10;
    let q = Number(n)*10;
    if(q>this.comDetls.length){
      q = this.comDetls.length;
    }
    for(let i = p; i < q; i++){
      this.comDetlsShow.push(this.comDetls[i]);
    }
    document.getElementById('showingResult').innerHTML ="Showing Result "+p+"-"+q+" of "+this.comDetls.length;
    console.log(this.comDetlsShow); 
  }
///pagination

  addCompany(){
    if(this.clientID){
      this.router.navigate(['/admin/add-company', {cID:this.clientID}]);
    }else{
      this.router.navigateByUrl('/client/company');
    }
  }

  companyDetails(cid){
    if(this.clientID || this.admin){
      if(this.clientID){
        this.router.navigate(['/admin/company-details', {cid:cid, redirect:'admin/company-list;c_id='+this.clientID}]);
      }else{
        this.router.navigate(['/admin/company-details', {cid:cid, redirect:'admin/company-list;admin=true'}]);
      }
    }else{
      this.router.navigate(['/client/company-details', {cid:cid}]);
    }
  }

  addJob(id, cid){
    if(this.clientID){
      this.router.navigate(['/admin/job-post', {id:id,cid:this.clientID}]);
    }else if(this.admin){
      this.router.navigate(['/admin/job-post', {id:id,cid:cid}]);
    }else{
      this.router.navigate(['/client/job-post', {id:id}]);
    }
  }

  comJobList(id, cid){
    if(this.clientID){
      this.router.navigate(['/admin/job-listing', {id:id,cid:this.clientID}]);
    }else if(this.admin){
      this.router.navigate(['/admin/job-listing', {id:id,cid:cid}]);
    }else{
      this.router.navigate(['/client/job-listing', {id:id}]);
    }
  }

}
