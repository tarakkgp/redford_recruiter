import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import { NgForm } from '@angular/forms';
import {LoadingController,ModalController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Router,ActivatedRoute} from '@angular/router';
import {AuthService,FacebookLoginProvider,GoogleLoginProvider} from 'angular-6-social-login';
import {LoginModalpagePage} from '../login-modalpage/login-modalpage.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public active_div:boolean=true;
  companytypelist:any;
  companysizelist:any;
  firstname:any;
  lastname:any;
  email_reg:any;
  phone:any;
  ccode:any;
  cname:any;
  csize:any;
  ctype:any;
  password_reg:any;
  cpassword:any;
  uname:any;
  upassword:any;
  checked:boolean=false;
  userchecked:boolean=false;
  size:any;
  type:any;
  errorMsg:any='';
  errorMsgS:any='';
  register: NgForm;

  constructor(private service: DataserviceService,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private router: Router,
    private socialAuthService: AuthService,
    public modalCtrl: ModalController,
    public acroute:ActivatedRoute) { }

  ngOnInit() {
    this.service.clientcompany_size().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]=="true"){
        this.companysizelist=resArray[2];
      }
    });
    
    this.service.get_method_apicall('client-registration-form').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]==true){
        this.companytypelist= resArray[2];
        console.log(this.companytypelist);
      }
      //this.service.hideLoader();
    });
  }


  // active_div(){
  //   if(this.active_div){
  //     this.active_div = false;
  //   }else{
  //     this.active_div = true;
  //   }
  // }

  registerDiv() {
    this.active_div = false;
    this.errorMsg='';
  }

  signinDiv() {
    this.active_div = true;
  }

  async validateuser(form: NgForm){   
    console.log(form);
    this.register= form;
    if (form.invalid) {
        //alert('invalid');
      return;
    }
   let postdata = {
    "data":{"first_name":this.firstname,
    "last_name":this.lastname,
    "company_name":this.cname,
    "company_size":this.csize,
    "company_type":this.ctype,
    "mobile":this.phone,
    "mob_country_code":this.ccode,
    "email":this.email_reg,
    "password":this.password_reg,
    "terms":this.checked,
    }
    }; 
    if(this.password_reg==this.cpassword){
      console.log(postdata);
      const loading = await this.loadingCtrl.create({
        message: ' Please wait. . .',
      });
      loading.present();
      this.service.alllogin_registration(postdata,"client-registration").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          //this.errorMsgS = resArray[1];
          this.storage.set("login_credential",result).then(()=>{
            this.service.get_login_credential();
            if(this.acroute.snapshot.paramMap.get('redirect')=="company"){
              this.router.navigateByUrl('/client/company-list');
            }else{
              this.router.navigateByUrl('/client/profile');
            }
            form.reset();
          });
        }else{
          this.errorMsg = resArray[1]
        }
        loading.dismiss();
      });
    }else{
      this.errorMsgS = "Password not same as confirm password!";
    }  
  }

  focusOut(){
    if(this.password_reg!=this.cpassword){
      this.errorMsgS = "Password not same as confirm password!";
    }else{
      this.errorMsgS = "No error!";
    }  

  }
  
  async validatelogin(form: NgForm){
    console.log(form);
    this.register= form;
    if (form.invalid) {
        //alert('invalid');
      return;
    }
   let postdata = {
    "data":{"username":this.uname,
    "password":this.upassword
    }
    }; 
      console.log(postdata);
      // const loading = await this.loadingCtrl.create({
      //   message: ' Please wait. . .',
      // });
      // loading.present();
      this.service.alllogin_registration(postdata,"client-signin").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          this.storage.set("login_credential",result).then(()=>{
            this.service.get_login_credential();
            if(this.acroute.snapshot.paramMap.get('redirect')=="company"){
              this.router.navigateByUrl('/client/company-list');
            }else{
              this.router.navigateByUrl('/client/profile');
            }
            
            form.reset();
            this.errorMsg = '';
            this.errorMsgS = '';
          });
        }else{
          this.errorMsg = resArray[1];
          //this.service.presentToast(resArray[0]);
        }
    }, error=>{
      this.service.presentToast('Server or network issue');
    });
    // loading.dismiss();
  }

  ionViewWillEnter(){
    this.storage.get('login_credential').then((val)=>{
      if(val){
        this.router.navigateByUrl('/client/profile');
      }
    });
    
  }
  
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }/*  else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    } */
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);

        this.service.alllogin_registration(userData,"client-provider-signin").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            if(resArray[3]=="true"){
              this.storage.set("login_credential",result).then(()=>{
                this.service.get_login_credential();
                if(this.acroute.snapshot.paramMap.get('redirect')=="company"){
                  this.router.navigateByUrl('/client/company-list');
                }else{
                  this.router.navigateByUrl('/client/profile');
                }
              });
            }else{
                this.editRegistration(resArray[2]);
            }  
          }else{
            this.service.Validalert(resArray[1]);
          }
        });  
            
      }
    );
  }

   public socialRegistration(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }/*  else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    } */
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {  
        console.log(socialPlatform+" sign in data : " , userData);
        this.service.alllogin_registration(userData,"client-provider-registration").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            //this.service.presentToast(resArray[1]);
            this.editRegistration(resArray[4]);
          }else{
            //this.dataservice.presentToast(resArray[0]);
            this.service.Validalert(resArray[1]);
          }
        });
            
      }
    );
  }

  forgot_password(){
    this.router.navigate(['/forgot-password',{type:'client'}]);
  }

  async editRegistration(cad){
    const modal = await this.modalCtrl.create({
      component: LoginModalpagePage,
      componentProps:{data:cad},
      cssClass : 'candidate_modal',
      showBackdrop: true,
      backdropDismiss:false
    });
    /* modal.onDidDismiss().then(details => {
    if(details.data){
      this.dataserv.updateAddress(details.data);
      this._zone.run(() => {
        this.initialise();
      });
    }
  }); */
  return await modal.present();
}

ionViewWillLeave(){
  if(this.register){
    this.register.reset();
  }
  this.active_div=true;
}
  
}
