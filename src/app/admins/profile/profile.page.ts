import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  looking_edit:boolean=true;
  countrylist:any;
  statelist:any;
  cmpdetails:any;
  showmail:any;
  showfirstname:any;
  showlastname:any;
  showgender:any;
  showphone:any;
  showphonecode:any;
  showimg:any;
  showalterphone:any;
  showalterphonecode:any;
  showaltermail:any;
  showaddress:any;
  showcity:any;
  showstate:any;
  showcountry:any;
  shownationality:any;
  country:any;
  state:any;

  extension:any;
  rdrRslt:any;

  name:any;
  lastname:any;
  gender:any;
  phonecode:any;
  phone:any;
  allphonecode:any;
  allphone:any;
  email:any;
  allemail:any;
  address:any;
  city:any;
  comCountry:any;
  comState:any;
  editedimgurl:any;
  gen:any;
  nationlist:any;
  Nationality:any;
  nanty:any;
  //change password...
  currentpass:any;
  newpass:any;
  confpas:any;
  errorMsgS:boolean=false;;
  admin:boolean=false;
  CID;

  constructor(
    public service: DataserviceService,
    public storage: Storage,
    public acRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.fetchProfileInfo();

    this.service.getCountry().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      this.countrylist=resArray[0];
    });

    this.service.getNationality().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      this.nationlist=resArray[0];
      console.log(resArray);
    });
  }

  fetchProfileInfo(){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{
            "admin_id":this.service.userid,
          }
        };    
        this.service.post_request_api_call(postdata,'admin-profile-information').then((result)=>{
          console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.cmpdetails=resArray[2];
            this.showmail=this.cmpdetails.username;
            this.name=this.cmpdetails.name;
            // this.showlastname=this.cmpdetails.last_name;
            // this.showgender=this.cmpdetails.gender;
            this.showphone=this.cmpdetails.mobile;
            this.showphonecode=this.cmpdetails.mob_country_code;
            this.showalterphone=this.cmpdetails.alternate_mobile;
            this.showalterphonecode=this.cmpdetails.alt_mob_country_code;
            this.showimg=this.cmpdetails.admin_profile_picture;
            this.showaltermail=this.cmpdetails.alternate_email;
            this.showaddress=this.cmpdetails.address;
            // this.showcity=this.cmpdetails.city;
            // this.showstate=this.cmpdetails.state;
            // this.showcountry=this.cmpdetails.country;
            // this.shownationality=this.cmpdetails.nationality;
            // if(this.acRoute.snapshot.paramMap.get('edit')){
            //   this.lookingForEdit();
            // }
          }
        });
      }
    });
  }

  lookingForEdit(){
    this.looking_edit = false;
    this.showmail=this.cmpdetails.username;
    this.name=this.cmpdetails.name;
    //this.lastname=this.cmpdetails.last_name;
    // if(this.cmpdetails.gender){
    //   console.log("gender");
    //   this.gender=this.cmpdetails.gender;
    //   console.log(this.gender)
    // }
    
    this.phone=this.cmpdetails.mobile;
    this.phonecode=this.cmpdetails.mob_country_code;
    this.allphone=this.cmpdetails.alternate_mobile;
    this.allphonecode=this.cmpdetails.alt_mob_country_code;
    this.rdrRslt = this.cmpdetails.admin_profile_picture;
    this.editedimgurl = this.cmpdetails.admin_profile_picture;
    this.email=this.cmpdetails.username;
    this.allemail=this.cmpdetails.alternate_email;
    this.address=this.cmpdetails.address;
    // this.city=this.cmpdetails.city;
    // if(this.cmpdetails.nationality){
    //   this.Nationality = this.cmpdetails.nationality;
    // }

    // if(this.cmpdetails.country){
    //   this.comCountry=this.cmpdetails.country;
    //   this.onChange(this.cmpdetails.country);
    // }

    // if(this.cmpdetails.state){
    //   this.comState= this.cmpdetails.state;
    // }

  }

  cancel_lookingfor(form:NgForm){
    form.reset();
    this.looking_edit = true;
  }

  changeImg(ev) {
    let file = ev.files[0];
    let file_name=file.name;
    this.extension=file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
    if(file.type=="image/jpeg" || file.type=="image/png"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        //console.log(reader.result);
        this.rdrRslt = reader.result;
      }
      reader.readAsDataURL(file);
    }else{
      this.service.Validalert("Only .jpg and .png file type are allowed.");
      ev.value='';
      //this.rdrRslt = '';
    }  
  }

  uploadIMG(){
    document.getElementById('videoInput').click();
  }

  validate_profile(job:NgForm){
    console.log(job);
    if (job.invalid || this.newpass != this.confpas) {
        //alert('invalid');
      return;
    }
    if(this.editedimgurl== this.rdrRslt){
      this.rdrRslt=''
    }
    console.log(this.admin);
    let postData = {
      "data":{
        "admin_id": this.service.userid,
        "name": this.name,
        "email": this.email,
        "alternate_email": this.allemail,
        "mob_country_code": this.phonecode,
        "mobile": this.phone,
        "alt_mob_country_code": this.allphonecode,
        "alternate_mobile": this.allphone,
        "address": this.address,
        "profile_pic": this.rdrRslt?this.rdrRslt:'',
        "file_extension": this.extension,
      }
     }
    console.log(postData);
    this.service.post_request_api_call(postData, 'admin-profile-update').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.showalert(resArray[2],'')
      }
    })

  }

  validate_password(pass:NgForm){
    console.log(pass);
    if (pass.invalid || this.newpass != this.confpas) {
        //alert('invalid');
      return;
    }

    let postData = {
      "data":{
        "admin_id": this.admin?this.CID:this.service.userid,
        "current_password": this.currentpass,
        "new_password": this.newpass 
      }
     };
    console.log(postData);
    this.service.post_request_api_call(postData, 'admin-password-update').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.showalert(resArray[2],'')
      }else{
        this.service.Validalert(resArray[2]);
      }
    })
  }

  focusOut(){
    if(this.newpass!=this.confpas){
      this.errorMsgS = true;
    }else{
      this.errorMsgS = false;
    }  

  }

}
