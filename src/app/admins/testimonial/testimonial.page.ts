import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.page.html',
  styleUrls: ['./testimonial.page.scss'],
})
export class TestimonialPage implements OnInit {
  companyname:any;
  name:any;
  comment:any;
  rdrRslt:any;
  extension: any;
  status:any="Select status";
  testid:any;
  updatedimg:any;
  constructor(public loadingCtrl: LoadingController,
    private dataservice: DataserviceService,
    private storage: Storage,
    public router: Router,
    public acroute: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    if(this.acroute.snapshot.paramMap.get("id")){
      
      this.testid = this.acroute.snapshot.paramMap.get("id");
      console.log(this.testid);
      this.getDetailsTestimonial(this.acroute.snapshot.paramMap.get("id"))
    }
  }
  
  getDetailsTestimonial(d){
    let postData = {
      "data":{
        "testimonial_id": d
      }
     }
    this.dataservice.post_request_api_call(postData, 'testimonial-details').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.name=resArray[2].user_name;
        this.companyname=resArray[2].company_name;
        this.comment=resArray[2].content;
        this.rdrRslt=resArray[2].user_image;
        this.updatedimg=this.rdrRslt;
        this.status=resArray[2].status;
      }
    })
  }

  async validate_test(test: NgForm){
    
    if(test.invalid){
      return;
    }else{
      // this.storage.remove('login_credential').then(()=>{
        if(this.rdrRslt){
          /* const loading = await this.loadingCtrl.create({
            message: ' Please wait. . .',
          });
          loading.present(); */
          let postdata = {
            "data":{"company_name":this.companyname,
            "name":this.name,
            "content":this.comment,
            "status" :this.status,
            "image":this.rdrRslt!=this.updatedimg?this.rdrRslt:'',
            "extension":this.extension,
            "testimonial_id": this.testid?this.testid:''
          }
          };
          this.dataservice.alllogin_registration(postdata,"testimonial-upload").then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            console.log(resArray);
            if(resArray[1]){
              test.onReset();
              this.status="Select status";
              this.rdrRslt="";
              this.dataservice.showalert(resArray[2],"/admin/testimonial-listing");
            }
            //loading.dismiss();
          });
        }else{
          this.dataservice.Validalert("Please choose an image!")
        }  
    }
  }
  
  changeImg(ev) {  
    let file = ev.files[0];
    let file_name=file.name;
    this.extension=file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
    if(file.type=="image/jpeg" || file.type=="image/png"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result);
        this.rdrRslt = reader.result;
      }
      reader.readAsDataURL(file);
    }else{
      //console.log("lllllllllllll");
      this.dataservice.Validalert("Only .jpg and .png file type are allowed.");
      ev.value='';
    }  
  }

  uploadIMG(){
    //console.log("click");
    document.getElementById('videoInput').click();
  }

  gotolist(){
    console.log("clickkkkk")
    this.router.navigateByUrl("/admin/testimonial-listing");
  }


}
