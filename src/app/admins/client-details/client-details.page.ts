import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataserviceService } from '../../Service/dataservice.service';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.page.html',
  styleUrls: ['./client-details.page.scss'],
})
export class ClientDetailsPage implements OnInit {

username;
client_profile_picture;
first_name;
last_name;
alternate_email;
mob_country_code;
mobile;
alt_mob_country_code;
alternate_mobile;
address;
city;
state;
country;
gender;
nationality;

  constructor(
    public acRoute: ActivatedRoute,
    public service: DataserviceService,
    public router: Router
  ) { }

  ngOnInit() {
    let cid = this.acRoute.snapshot.paramMap.get('c_id');
    if(cid){
      let postData = {
        "data":{
          "client_id": cid
        }
       }
      this.service.post_request_api_call(postData, 'client-details').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.username = resArray[2].username;
          this.client_profile_picture = resArray[2].client_profile_picture;
          this.first_name = resArray[2].first_name;
          this.last_name = resArray[2].last_name;
          this.alternate_email = resArray[2].alternate_email;
          this.mob_country_code = resArray[2].mob_country_code;
          this.mobile = resArray[2].mobile;
          this.alt_mob_country_code = resArray[2].alt_mob_country_code;
          this.alternate_mobile = resArray[2].alternate_mobile;
          this.address = resArray[2].address;
          this.city = resArray[2].city;
          this.state = resArray[2].state;
          this.country = resArray[2].country;
          this.gender = resArray[2].gender;
          this.nationality = resArray[2].nationality;
        }
      });
    }
  }

  editClient(){
    this.router.navigateByUrl('/admin/edit-client');
  }

}
