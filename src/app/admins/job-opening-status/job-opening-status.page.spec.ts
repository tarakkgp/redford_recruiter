import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobOpeningStatusPage } from './job-opening-status.page';

describe('JobOpeningStatusPage', () => {
  let component: JobOpeningStatusPage;
  let fixture: ComponentFixture<JobOpeningStatusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobOpeningStatusPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobOpeningStatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
