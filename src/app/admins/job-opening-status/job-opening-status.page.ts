import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-job-opening-status',
  templateUrl: './job-opening-status.page.html',
  styleUrls: ['./job-opening-status.page.scss'],
})
export class JobOpeningStatusPage implements OnInit {
  jobstatustitle: any;
  status:any="Select Status";
  jobstatuslist:any;
  jobstatusid:any="";
  sequence:any;

  constructor(public service: DataserviceService,
    public storage: Storage,
    public alertController: AlertController) { }

  ngOnInit() {
    this.call_jobopeningstatus_api();
  }

  async validatejob(form: NgForm){
    console.log(form);
    if (form.invalid || this.status=="Select status") {
        //alert('invalid');
      return;
    }
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{ "title": this.jobstatustitle,
          "status": this.status,
          "user_id": this.service.userid,
          "sequence":this.sequence,
          "job_opening_status_id": this.jobstatusid?this.jobstatusid:"" 
          }
          }; 
            console.log(postdata);
            // const loading = await this.loadingCtrl.create({
            //   message: ' Please wait. . .',
            // });
            // loading.present();
            this.service.post_request_api_call(postdata,"job-opening-status-upload").then((result)=>{
              const resArray = Object.keys(result).map(i => result[i]);
              console.log(resArray);
              if(resArray[1]){
                form.onReset();
                this.status = "Select Status";
                this.jobstatusid="";
                this.service.Successalert(resArray[2]);
                this.call_jobopeningstatus_api();
              }else{
                this.service.Validalert(resArray[2]);
              }
              
          }, error=>{
            this.service.presentToast('Server or network issue');
          });
    // loading.dismiss();
      }
    });      
  }

  call_jobopeningstatus_api(){
    this.service.post_request_api_call(null,"job-opening-status-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log('jobstatuslist',resArray);
      if(resArray[1]){
        this.jobstatuslist=resArray[2].sort(function(a, b){
            return a.sequence-b.sequence
        })
        console.log('jobstatuslist',this.jobstatuslist);
      }
  }, error=>{
    this.service.presentToast('Server or network issue');
  });
  }

  edit_jobstatus(id,title,stat,sc){
    this.jobstatusid=id;
    this.jobstatustitle =title;
    this.status=stat;
    this.sequence=sc;
  }

  deletefavouritejob(d){
    let postData = {
      "data":{
        "job_opening_status_id": d
      }
     }
    this.service.post_request_api_call(postData, 'job-opening-status-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.showalert(resArray[2],"");
        //this.call_jobopeningstatus_api();
      }
    })
  }

  async DeleteConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deletefavouritejob(id);
          }
        }
      ]
    });

    await alert.present();
  }

}
