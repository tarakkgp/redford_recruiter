import { Component, OnInit } from '@angular/core';
import { MenuController, PopoverController } from '@ionic/angular';
import {Router} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  adminData;
  client_count;
  company_count;
  candidate_count;
  job_count;

  constructor(
    public menu: MenuController,
    public popoverCtrl:PopoverController,
    public router: Router,
    public service: DataserviceService
  ) { }

  ngOnInit() {
    let postData = {
      "data":{
        "admin_id": this.service.userid,
      }
    }
    //console.log(postData);
    this.service.post_request_api_call(postData, 'admin-dashboard').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      //console.log(res,resArray);
      if(resArray[1]){
        this.adminData = res;
        this.client_count = resArray[2];
        this.company_count = resArray[3];
        this.candidate_count = resArray[4];
        this.job_count = resArray[5];
        //this.calculatePage(resArray[2].length); ///pagination
      }
    })
  }

  gotoClient(){
    this.router.navigateByUrl('/admin/clients');
  }

  gotoCompany(){
    this.router.navigate(['/admin/company-list', {admin:true}]);
  }

  gotoCandidate(){
    this.router.navigateByUrl('/admin/candidates');
  }

  gotoProfile(){
    this.router.navigateByUrl('/admin/profile');
  }

}


