import { Component, OnInit, ViewChild } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.page.html',
  styleUrls: ['./clients.page.scss'],
})
export class ClientsPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  comDetls = [];
  pages = []; ///pagination
  comDetlsShow = []; ///pagination
  start:number = 0;
  limit:number = 10;
  totaldata;

  constructor(
    public service: DataserviceService,
    public router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {
    let postdata = {
      "data":{
        "start": this.start,
        "limit": this.limit
      }
    }
    this.service.post_request_api_call(postdata, 'client-list').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[0]){
        resArray[1].forEach(element => {
          this.comDetls.push(element);
        });
        this.virtualScroll.checkEnd();
        this.totaldata = resArray[2];
        //this.calculatePage(resArray[1].length); ///pagination
      }
    });
  }

  loadData(event) {
    this.start = this.start+this.limit;
    setTimeout(() => {
    
      this.ngOnInit()
      //Hide Infinite List Loader on Complete
      event.target.complete();

      //Rerender Virtual Scroll List After Adding New Data

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.start+this.limit >= this.totaldata) {
        event.target.disabled = true;
      }
    }, 500);
  }

  ionViewDidEnter(){
    
  }

  ///pagination
  calculatePage(totalData){
    this.pages = [];
    let totalPages = Math.floor(totalData/10)+1;
    for(let i = 0; i < totalPages; i++){
      this.pages.push(i+1);
    }
    this.showPagination(1);
  }

  showPagination(n){
    this.comDetlsShow = [];
    document.getElementById('showingResult').innerHTML = "";
    let p = (Number(n)-1)*10;
    let q = Number(n)*10;
    if(q>this.comDetls.length){
      q = this.comDetls.length;
    }
    for(let i = p; i < q; i++){
      this.comDetlsShow.push(this.comDetls[i]);
    }
    document.getElementById('showingResult').innerHTML ="Showing Result "+p+"-"+q+" of "+this.comDetls.length;
    console.log(this.comDetlsShow); 
  }
///pagination

  clientDetails(id){
    this.router.navigate(['/admin/client-details', {c_id:id}]);
  }

  companyList(id){
    this.router.navigate(['/admin/company-list', {c_id:id}]);
  }

  addClient(){
    this.router.navigate(['/admin/client-details', {admin:true}]);
  }

  async deleteClient(id){
    const alert = await this.alertController.create({
      header: 'Delete!',
      message: 'Do you want to delete this client?',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'secondary',
          role: "cancel"
        },
        {
          text: 'Ok',
          cssClass: 'secondary',
          handler: () => {
            this.deleteClientApi(id);
          }
        }
      ]
    });

    await alert.present();
  }

  deleteClientApi(id){
    let postData = {
      "data":{
        "client_id": id
      }
    }
    this.service.post_request_api_call(postData, 'client-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.showalert(resArray[2], '');
      }else{
        this.service.presentToast('There is an error');
      }
    });
  }

}
