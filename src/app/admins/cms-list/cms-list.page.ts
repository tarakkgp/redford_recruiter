import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-cms-list',
  templateUrl: './cms-list.page.html',
  styleUrls: ['./cms-list.page.scss'],
})
export class CmsListPage implements OnInit {
  cmslist:any;

  constructor(public router: Router,private dataservice: DataserviceService,
    public alertController: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.dataservice.post_request_api_call("","cms-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.cmslist = resArray[2];
      }
    }, error=>{
      this.dataservice.presentToast('Server or network issue');
    });
  }

  add_cms(){
    this.router.navigateByUrl('/admin/cms');
  }
  edit_cms(id){
    this.router.navigate(['/admin/cms',{cmsid:id}]);
  }

  Delete_cms(id){

  }

  deletecmspage(d){
    let postData = {
      "data":{
        "cms_id": d
      }
     }
    this.dataservice.post_request_api_call(postData, 'cms-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.dataservice.showalert(resArray[2],"");
        //this.call_jobopeningstatus_api();
      }
    })
  }

  async DeleteConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deletecmspage(id);
          }
        }
      ]
    });

    await alert.present();
  }

}
