import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.page.html',
  styleUrls: ['./candidates.page.scss'],
})
export class CandidatesPage implements OnInit {

  canList;

  constructor(
    public service: DataserviceService,
    public router: Router
  ) { }

  ngOnInit() {
    this.service.get_method_apicall('candidate-listing-for-admin').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.canList = resArray[2];
      }
    })
  }

  canDetails(id){
    this.router.navigate(['/admin/candidate-details', {c_id:id}]);
  }

}
