import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataserviceService } from '../../Service/dataservice.service';

@Component({
  selector: 'app-candidate-details',
  templateUrl: './candidate-details.page.html',
  styleUrls: ['./candidate-details.page.scss'],
})
export class CandidateDetailsPage implements OnInit {

  canDetails;

  constructor(
    public acRoute: ActivatedRoute,
    public service: DataserviceService
  ) { }

  ngOnInit() {
    let cid = this.acRoute.snapshot.paramMap.get('c_id');
    if(cid){
      let postData = {
        "data":{
          "candidate_id": cid
        }
      }
      this.service.post_request_api_call(postData, 'candidate-details-for-admin').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.canDetails = resArray[2];
        }
      })
    }
  }

}
