import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataserviceService } from '../../Service/dataservice.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.page.html',
  styleUrls: ['./company-details.page.scss'],
})
export class CompanyDetailsPage implements OnInit {

  comDetails;

  constructor(
    public acRoute: ActivatedRoute,
    public router: Router,
    public service: DataserviceService
  ) { }

  ngOnInit() {
    let postData = {
      "data":{
        "company_id": this.acRoute.snapshot.paramMap.get('cid')
      }
     }
    this.service.post_request_api_call(postData, 'company-details').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.comDetails = resArray[2];
        // console.log(this.details.company_name);
      }
    })
  }

}
