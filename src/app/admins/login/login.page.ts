import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  uname:any;
  password:any;
  errMsg;

  constructor(
    public loadingCtrl: LoadingController,
    private dataservice: DataserviceService,
    private storage: Storage,
    public router: Router
  ) { }

  ngOnInit() {
  }

  async validateuser(form: NgForm){
    //console.log(form);
    if(form.invalid){
      //alert('invalid');
      return;
    }else{
      // this.storage.remove('login_credential').then(()=>{
        const loading = await this.loadingCtrl.create({
          message: ' Please wait. . .',
        });
        loading.present();
        let postdata = {
          "data":{"username":this.uname,"password":this.password}
        };
        this.dataservice.alllogin_registration(postdata,"admin-signin").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log('Admin Credential///',resArray);
          if(resArray[0]=="true"){
            //this.storage.set("adminuser",resArray);
            this.storage.set("login_credential", result).then(()=>{
              this.dataservice.get_login_credential();
              this.router.navigateByUrl('/admin/dashboard');
            })
          }else{
            //this.dataservice.presentToast(resArray[0]);
            this.errMsg = resArray[1];
          }
          loading.dismiss();
        });
      // });
    }
  } 

  forgot_password(){
    this.router.navigate(['/forgot-password',{type:'admin'}]);
  }

}
