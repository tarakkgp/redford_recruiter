import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CmsPage } from './cms.page';
import { ComponentsModule } from '../../components/components.module';
import { CKEditorModule } from 'ngx-ckeditor';

const routes: Routes = [
  {
    path: '',
    component: CmsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    CKEditorModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CmsPage]
})
export class CmsPageModule {}
