import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {Router,ActivatedRoute} from "@angular/router"

@Component({
  selector: 'app-cms',
  templateUrl: './cms.page.html',
  styleUrls: ['./cms.page.scss'],
})
export class CmsPage implements OnInit {
  title:any;
  editorValue:any;
  status:any="Select status";
  id:any;
  constructor(public service: DataserviceService,
    public storage: Storage,
    public router:Router,
    public acroute: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    if(this.acroute.snapshot.paramMap.get("cmsid")){
      this.id=this.acroute.snapshot.paramMap.get("cmsid");

      let postdata = {
        "data":{ "cms_id": this.id
        }
        }; 
          console.log(postdata);
        this.service.post_request_api_call(postdata,"cms-details").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.title=resArray[2].title;
            this.editorValue=resArray[2].content;
            this.status = resArray[2].status;
          }
        }, error=>{
          this.service.presentToast('Server or network issue');
        });
    }
  }

  async validatejob(form: NgForm){
    console.log(form);
    if (form.invalid || this.status=="Select status") {
        //alert('invalid');
      return;
    }
    let postdata = {
      "data":{ "title": this.title,
      "content": this.editorValue,
      "status": this.status,
      "cms_id": this.id?this.id:''
      }
      }; 
        console.log(postdata);
        // const loading = await this.loadingCtrl.create({
        //   message: ' Please wait. . .',
        // });
        // loading.present();
      this.service.post_request_api_call(postdata,"cms-upload").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.service.Successalert(resArray[2]);
          form.onReset();
          this.status = "Select status";
          this.router.navigateByUrl('/admin/cms-list');
        }
      }, error=>{
        this.service.presentToast('Server or network issue');
      });
      // loading.dismiss();
  }
  

}
