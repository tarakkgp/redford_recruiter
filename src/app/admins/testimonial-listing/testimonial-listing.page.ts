import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-testimonial-listing',
  templateUrl: './testimonial-listing.page.html',
  styleUrls: ['./testimonial-listing.page.scss'],
})
export class TestimonialListingPage implements OnInit {
  testlist:any;

  constructor(public router: Router,private dataservice: DataserviceService,
    public alertController: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.dataservice.post_request_api_call("","testimonial-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.testlist=resArray[2];
      }
    });
  }

  add_testimonial(){
    this.router.navigateByUrl('/admin/testimonial');
  }

  async DeleteConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            this.deletetstimonial(id);
          }
        }
      ]
    });

    await alert.present();
  }

  deletetstimonial(d){
    let postData = {
      "data":{
        "testimonial_id": d
      }
     }
    this.dataservice.post_request_api_call(postData, 'testimonial-delete').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.dataservice.showalert(resArray[2],"");
        //this.call_jobopeningstatus_api();
      }
    })
  }

  edit_testimonial(testid){
    this.router.navigate(['/admin/testimonial',{id:testid}])
  }

}
