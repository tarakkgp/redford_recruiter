import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';


@Component({
  selector: 'app-candidate-job-status',
  templateUrl: './candidate-job-status.page.html',
  styleUrls: ['./candidate-job-status.page.scss'],
})
export class CandidateJobStatusPage implements OnInit {
  jobstatustitle: any;
  status:any="Select Status";
  jobstatuslist:any;
  sequence:any;
  mainstatus:any="Select Status Title";
  job_sub_statustitle:any;
  sub_status:any="Select Status"
  sequen:any;
  candidate_jobstatusid:any="";
  mainstatuslist: any;
  candidate_substatusid="";


  constructor(public service: DataserviceService,
    public storage: Storage,
    public alertController: AlertController) { }

    ngOnInit() {
      this.call_jobopeningstatus_api();
      this.jobopening_statuslist();
    }
  
    async validatejob(form: NgForm){
      console.log(form);
      if (form.invalid || this.status=="Select Status") {
          //alert('invalid');
        return;
      }
      this.storage.get("login_credential").then((val)=>{
        if(val){
          let postdata = {
            "data":{ "title": this.jobstatustitle,
            "status": this.status,
            "user_id": this.service.userid,
            "sequence":this.sequence,
            "candidate_job_opening_status_id": this.candidate_jobstatusid?this.candidate_jobstatusid:"" 
            }
            }; 
              console.log(postdata);
              // const loading = await this.loadingCtrl.create({
              //   message: ' Please wait. . .',
              // });
              // loading.present();
              this.service.post_request_api_call(postdata,"candidate-job-opening-status-upload").then((result)=>{
                const resArray = Object.keys(result).map(i => result[i]);
                console.log(resArray);
                if(resArray[1]){
                  form.onReset();
                  this.status = "Select Status";
                  this.candidate_jobstatusid="";
                  this.service.Successalert(resArray[2]);
                  this.call_jobopeningstatus_api();
                  this.jobopening_statuslist();
                }else{
                  this.service.Validalert(resArray[2]);
                }
                
            }, error=>{
              this.service.presentToast('Server or network issue');
            });
      // loading.dismiss();
        }
      });      
    }
  
    call_jobopeningstatus_api(){
      this.service.post_request_api_call(null,"candidate-job-opening-status-list").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.jobstatuslist=resArray[2];
          console.log(this.jobstatuslist);
        }
    }, error=>{
      this.service.presentToast('Server or network issue');
    });
    }
  
    edit_jobstatus(id,title,stat,sc){
      this.candidate_jobstatusid=id;
      this.jobstatustitle =title;
      this.status=stat;
      this.sequence=sc;
    }
  
    deletefavouritejob(d){
      let postData = {
        "data":{
          "candidate_job_opening_status_id": d
        }
       }
      this.service.post_request_api_call(postData, 'candidate-job-opening-status-delete').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.service.showalert(resArray[2],"");
          //this.call_jobopeningstatus_api();
        }
      })
    }
  
    async DeleteConfirm(id) {
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: 'Are you want to delete!',
        cssClass: 'langDelete',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ok',
            handler: () => {
              this.deletefavouritejob(id);
            }
          }
        ]
      });
  
      await alert.present();
    }

    jobopening_statuslist(){
      this.service.post_request_api_call(null,"candidate-job-opening-status-dropdown").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.mainstatuslist=resArray[2];
          console.log(this.mainstatuslist);
        }
    }, error=>{
      this.service.presentToast('Server or network issue');
    });
    }

    validatesubstatus(cjs:NgForm){
      console.log(cjs);
      if (cjs.invalid || this.sub_status=="Select Status" || this.mainstatus=="Select Status Title") {
          //alert('invalid');
        return;
      }
      this.storage.get("login_credential").then((val)=>{
        if(val){
          let postdata = {
            "data":{ "title": this.job_sub_statustitle,
            "status": this.sub_status,
            "user_id": this.service.userid,
            "sequence":this.sequen,
            "candidate_job_opening_status_id": this.mainstatus,
            "candidate_job_opening_sub_status_id": this.candidate_substatusid?this.candidate_substatusid:""
            }
            }; 
              console.log(postdata);
              // const loading = await this.loadingCtrl.create({
              //   message: ' Please wait. . .',
              // });
              // loading.present();
              this.service.post_request_api_call(postdata,"candidate-job-opening-sub-status-upload").then((result)=>{
                const resArray = Object.keys(result).map(i => result[i]);
                console.log(resArray);
                if(resArray[1]){
                  cjs.onReset();
                  this.sub_status = "Select Status";
                  this.mainstatus ="Select Status Title";
                  this.candidate_substatusid="";
                  this.service.Successalert(resArray[2]);
                  this.call_jobopeningstatus_api();
                }else{
                  this.service.Validalert(resArray[2]);
                }
                
            }, error=>{
              this.service.presentToast('Server or network issue');
            });
      // loading.dismiss();
        }
      }); 

    }
    
    async DeleteSubstatus(id) {
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: 'Are you want to delete!',
        cssClass: 'langDelete',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ok',
            handler: () => {
              this.deletesStatusapi(id);
            }
          }
        ]
      });
  
      await alert.present();
    }

    deletesStatusapi(d){
      let postData = {
        "data":{
          "candidate_job_opening_sub_status_id": d
        }
       }
      this.service.post_request_api_call(postData, 'candidate-job-opening-sub-status-delete').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.service.showalert(resArray[2],"");
          //this.call_jobopeningstatus_api();
        }
      })
    }

    editSubstatus(candidate_job_opening_status_id,candidate_job_opening_sub_status_id,title,statu,sequenc){
      this.mainstatus = candidate_job_opening_status_id;
      this.candidate_substatusid = candidate_job_opening_sub_status_id;
      this.job_sub_statustitle = title;
      this.sub_status = statu;
      this.sequen = sequenc;  
    }


}
