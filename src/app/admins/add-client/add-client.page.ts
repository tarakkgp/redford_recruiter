import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {

  firstname:any;
  lastname:any;
  category:any;
  email:any;
  phone:any;
  ccode:any;
  password;
  cpassword;
  errorMsgS:boolean=false;

  categorylist;
  type:any;

  constructor(
    private service: DataserviceService,
    public dataservice: DataserviceService
  ) { }

  ngOnInit() {
    this.dataservice.get_method_apicall('candidate-category-list').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray)
      if(resArray[0]){
        this.categorylist= resArray[1];
      }
    });
  }

  async validateuser(form: NgForm){   
    if (form.invalid || this.errorMsgS) {
      //alert('invalid');
      return;
    }else{
      console.log(form);
      let postdata = {
        "data":{
        "first_name":this.firstname,
        "last_name":this.lastname,
        "mobile":this.phone,
        "mob_country_code":this.ccode,
        "email":this.email,
        "password":this.password,
        "category_id": this.category
        }
      };
      this.service.post_request_api_call(postdata,"candidate-addedit").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.service.showalert(resArray[2], '/admin/candidates');
        }
      });
    }
  }

  updateList(ev){
    if(ev.target.value!=this.password){
      this.errorMsgS = true;
    }else{
      this.errorMsgS = false;
    }
  }

}
