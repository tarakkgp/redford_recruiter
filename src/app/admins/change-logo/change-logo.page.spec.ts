import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeLogoPage } from './change-logo.page';

describe('ChangeLogoPage', () => {
  let component: ChangeLogoPage;
  let fixture: ComponentFixture<ChangeLogoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeLogoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeLogoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
