import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';

@Component({
  selector: 'app-change-logo',
  templateUrl: './change-logo.page.html',
  styleUrls: ['./change-logo.page.scss'],
})
export class ChangeLogoPage implements OnInit {

  rdrRslt;
  errMsg;
  extension;
  changeOn:boolean=false;

  constructor(
    public service: DataserviceService
  ) { }

  ngOnInit() {
    
  }

  ionViewDidEnter(){
    this.rdrRslt = this.service.mainLogo;
    //console.log(this.service.mainLogo);
  }

  uploadImg(){
    document.getElementById('videoInput').click();
  }

  changeImg(ev) {
    let file = ev.files[0];
    let file_name=file.name;
    this.extension=file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
      if(file.type=="image/jpeg" || file.type=="image/png"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        //console.log(reader.result);
        this.rdrRslt = reader.result;
        this.changeOn = true;
        this.errMsg = '';
      }
      reader.readAsDataURL(file);
    }else{
      //this.service.presentToast("Please select only .jpeg extension images.")
      this.errMsg = 'Please select JPG or PNG image'
    }  
  }

  confirmLogo(){
    let postData = {
      "data":{
        "website_logo":this.rdrRslt,
        "file_extension":this.extension
      }
    };
    this.service.post_request_api_call(postData, 'website-logo-upload').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        //this.errMsg = resArray[2];
        this.service.showalert(resArray[2], '')
        /* this.service.post_request_api_call('', 'website-logo-showing').then((res)=>{
          const resArray = Object.keys(res).map(i => res[i]);
          console.log(resArray);
          if(resArray[1]){
            this.service.mainLogo = resArray[2];
          }
        }) */
      }else{
        this.errMsg = resArray[2];
      }
    })
  }

}
