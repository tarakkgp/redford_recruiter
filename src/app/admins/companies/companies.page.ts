import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.page.html',
  styleUrls: ['./companies.page.scss'],
})
export class CompaniesPage implements OnInit {

  comDetls;
  noComMsg;

  constructor(
    public service: DataserviceService,
    public router: Router,
    public acRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    let clientID = this.acRoute.snapshot.paramMap.get('c_id');
    console.log(clientID);
    if(clientID){
      this.callCompanyList(clientID);
    }else{
      this.service.get_method_apicall('company-listing').then((res)=>{
        const resArray = Object.keys(res).map(i => res[i]);
        console.log(resArray);
        if(resArray[1]){
          this.comDetls = resArray[2];
        }else{
          this.noComMsg = resArray[2];
        }
      })
    }
  }

  callCompanyList(id){
    let postData = {
      "data":{
        "client_id": id
      }
     }
    this.service.post_request_api_call(postData, 'company-listing-for-client').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        this.comDetls = resArray[2];
      }else{
        this.noComMsg = resArray[2];
      }
    });
  }

  companyDetails(cid){
    this.router.navigate(['/admin/company-details', {cid:cid}]);
  }

  addJob(id){
    this.router.navigate(['/client/job-post', {id:id}]);
  }

  comJobList(id){
    this.router.navigate(['/client/job-listing', {id:id}]);
  }

}
