import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPopoverPage } from './status-popover.page';

describe('StatusPopoverPage', () => {
  let component: StatusPopoverPage;
  let fixture: ComponentFixture<StatusPopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusPopoverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusPopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
