import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {DataserviceService} from '../Service/dataservice.service'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  uname:any;
  usertype:any;
  errorMsg:any='';
  codes:boolean;
  sCodes:any;
  newPass:boolean;
  id:any;
  password:any;
  cpassword:any;
  errorMsgS:any;

  constructor(public acroute: ActivatedRoute,
    public service: DataserviceService,
    public router: Router) { }

  ngOnInit() {
    //console.log(this.acroute.snapshot.paramMap.get('type'));
    this.usertype =this.acroute.snapshot.paramMap.get('type'); 
  }

  async validatelogin(form: NgForm){
    console.log(form);
    if (form.invalid) {
        //alert('invalid');
      return;
    }
      let postdata = {
        "data":{"email":this.uname,
        "type": this.usertype
      }
      }; 
      console.log(postdata);
      this.service.post_request_api_call(postdata,"forgot-password").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.codes = true;
          document.getElementById('errorRed').style.color='green';
          this.errorMsg=resArray[2];
        }else{
          document.getElementById('errorRed').style.color='red';
          this.errorMsg=resArray[2];
        }
      });
  }

  async validateCode(form:NgForm){
    if (form.invalid) {
      //alert('invalid');
    return;
  }
    let postdata = {
      "data":{"email":this.uname,
      "type": this.usertype,
      "otp":this.sCodes
    }
    }; 
    console.log(postdata);
    this.service.post_request_api_call(postdata,"forgot-password-otp-check").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.newPass = true;
        this.id = resArray[3];
        this.errorMsg='';
        // if(this.usertype == 'candidate'){
        //   this.router.navigateByUrl('/candidate/login');
        // }else if(this.usertype == 'clint'){
        //   this.router.navigateByUrl('/clint/login');
        // }
        //this.codes = true;
      }else{
        this.errorMsg=resArray[2];
        document.getElementById('errorRed').style.color='red';
      }
    });
  }

  focusOut(){
    if(this.password!=this.cpassword){
      this.errorMsgS = "Password not same as confirm password!";
    }else{
      this.errorMsgS = "No error!";
    } 
  }

  async setNewPass(form: NgForm){
    console.log(form);
    if (form.invalid || this.password != this.cpassword) {
        //alert('invalid');
      return;
    }
      let postdata = {
        "data":{"email":this.uname,
        "type": this.usertype,
        "password":this.password,
        "user_id":this.id
      }
      }; 
      console.log(postdata);
      this.service.post_request_api_call(postdata,"forgot-password-update").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          //this.codes = true;
          this.errorMsg='';
          if(this.usertype == 'candidate'){
            this.service.showalert(resArray[2],'/candidate/login');
          }else if(this.usertype == 'client'){
            this.service.showalert(resArray[2],'/client/login');
          }else{
            this.service.showalert(resArray[2],'/admin/login');
          }
        }else{
          this.errorMsg=resArray[2];
        }
      });
  }

  ionViewDidEnter(){
    //this.uname = '';
    if(!this.codes){
      this.errorMsg = '';
    }
    //this.codes = false;
  }

}
