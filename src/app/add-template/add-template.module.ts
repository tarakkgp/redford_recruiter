import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsModule } from '../components/components.module';

import { IonicModule } from '@ionic/angular';

import { AddTemplatePage } from './add-template.page';
import { CKEditorModule } from 'ngx-ckeditor';

const routes: Routes = [
  {
    path: '',
    component: AddTemplatePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    CKEditorModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddTemplatePage]
})
export class AddTemplatePageModule {}
