import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {Router,ActivatedRoute} from "@angular/router"

@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.page.html',
  styleUrls: ['./add-template.page.scss'],
})
export class AddTemplatePage implements OnInit {

  title:any="Template Title";
  subject:any;
  editorValue:any;
  status:any="Select status";
  id:any;
  templateId;
  titleArrey;

  constructor(
    public service: DataserviceService,
    public storage: Storage,
    public router:Router,
    public acroute: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.service.post_request_api_call("","email-template-type-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]){
        this.titleArrey = resArray[2];
        console.log(this.titleArrey);
      }else{
        this.service.Successalert("Template Title do not load properly");
      }
    }, error=>{
      this.service.Successalert('Server or network relatede issue');
    });
    this.templateId = this.acroute.snapshot.paramMap.get('id')
    if(this.templateId){
      let postdata = {
        "data":{
          "email_template_id":this.templateId
        }
      };
      this.service.post_request_api_call(postdata,"email-template-details").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.title = resArray[2].email_template_type;
          this.subject = resArray[2].subject;
          this.editorValue = resArray[2].content;
          this.status = resArray[2].status;
          document.getElementById('temTitle').setAttribute('disabled', 'true');
        }else{
          this.service.Successalert("Unknown Error");
        }
      }, error=>{
        this.service.Successalert('Server or network relatede issue');
      });
    }
  }

  async validatejob(form: NgForm){
    // console.log(form);
    if (form.invalid || this.status=="Select status" || this.title == "Template Title") {
        //alert('invalid');
      return;
    }
    let postdata = {
      "data":{
        "email_template_id":this.templateId?this.templateId:'',
        "email_template_type": this.title,
        "subject":this.subject,
        "content":this.editorValue,
        "status":this.status,
        "user_id":this.service.userid
      }
    }; 
    console.log(postdata);
    this.service.post_request_api_call(postdata,"email-template-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        form.onReset();
        this.title = "Template Title";
        this.status = "Select status";
        this.service.showalert(resArray[2],'/admin/email-template');
      }else{
        this.service.Successalert(resArray[2]);
      }
    }, error=>{
      this.service.Successalert('Server or network relatede issue');
    });
  }

}
