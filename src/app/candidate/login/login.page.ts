import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import { NgForm } from '@angular/forms';
import {LoadingController,ModalController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService,FacebookLoginProvider,GoogleLoginProvider,LinkedinLoginProvider} from 'angular-6-social-login';
import {CandidateLoginModalpagePage} from '../candidate-login-modalpage/candidate-login-modalpage.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public active_div:boolean=true;
  firstname:any;
  lastname:any;
  email_reg:any;
  phone:any;
  ccode:any;
  cname:any;
  csize:any;
  ctype:any;
  password_reg:any;
  cpassword:any;
  uname:any;
  upassword:any;
  checked:boolean=false;
  userchecked:boolean=false;
  errorMsg:any='';
  errorMsgS:any='';
  categorylist:any;
  category:any;
  register:NgForm;
  type:any;
  

  constructor(private service: DataserviceService,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private router: Router,
    private socialAuthService: AuthService,
    public acRoute: ActivatedRoute,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    

  }

  // active_div(){
  //   if(this.active_div){
  //     this.active_div = false;
  //   }else{
  //     this.active_div = true;
  //   }
  // }

  registerDiv() {
    this.active_div = false;
    this.errorMsg='';
    this.service.get_method_apicall('category-list').then((result)=>{
      //console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]=="true"){
        this.categorylist= resArray[2];
        console.log(this.categorylist);
      }
      
    });
  }

  signinDiv() {
    this.active_div = true;
  }

  async validateuser(form: NgForm){   
    console.log(form);
    this.register=form;
    if (form.invalid) {
        //alert('invalid');
      return;
    }
    if(this.category!=0){
      let postdata = {
      "data":{"first_name":this.firstname,
      "last_name":this.lastname,
      "mobile":this.phone,
      "mob_country_code":this.ccode,
      "email":this.email_reg,
      "password":this.password_reg,
      "terms":this.checked,
      "category_id":this.category
      }
      }; 
    
      if(this.password_reg==this.cpassword){
        console.log(postdata);
      
        const loading = await this.loadingCtrl.create({
          message: ' please wait. . .',
        });
        loading.present();
        this.service.alllogin_registration(postdata,"candidate-registration").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            //this.errorMsgS = resArray[1];
            this.storage.set("login_credential",result).then(()=>{
              this.service.get_login_credential();
              this.router.navigateByUrl('/candidate/dashboard');
              form.reset();  

            });            
          }else{
            this.errorMsg = resArray[1];
          }
          loading.dismiss();
        });
      }else{
        this.errorMsgS = "Password not same as confirm password!";
      } 
    }else{
      this.errorMsg = "Please select any category!";
    } 
  }

  focusOut(){
    if(this.password_reg!=this.cpassword){
      this.errorMsgS = "Password not same as confirm password!";
    }else{
      this.errorMsgS = "No error";
    }  

  }
  
  async validatelogin(form: NgForm){
    console.log(form);
    this.register=form;
    if (form.invalid) {
        //alert('invalid');
      return;
    }
   let postdata = {
    "data":{"username":this.uname,
    "password":this.upassword
    }
    }; 
      console.log(postdata);
      /* const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present(); */
      this.service.alllogin_registration(postdata,"candidate-signin").then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        //console.log(JSON.parse(JSON.stringify(result)));
        console.log(resArray);
        //console.log(result.token);
        if(resArray[0]=="true"){
          this.storage.set("login_credential",result).then(()=>{
            // this.service.presentToast(resArray[0]);
            this.service.get_login_credential();
            if(this.acRoute.snapshot.paramMap.get('redirect')){
              this.router.navigate(['/job-details', {id:this.acRoute.snapshot.paramMap.get('redirect')}])
            }else{
              form.reset();
              this.errorMsg = '';
              this.errorMsgS = '';
              this.router.navigateByUrl('/candidate/dashboard');
            }
            
          });
        }else{
          //this.service.presentToast(resArray[0]);
          this.errorMsg = resArray[1];
        }
        // loading.dismiss();
      });
  }

  ionViewWillEnter(){
    let activetype = this.acRoute.snapshot.paramMap.get("type");
    if(activetype=="login"){
      this.active_div = true;
    }else{
      this.active_div = false;
      this. registerDiv();
    }
    this.storage.get('login_credential').then((val)=>{
      if(val){
        this.router.navigateByUrl('/candidate/dashboard');
      }
    });
    
  } 

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } /* else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    } */
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);

        this.service.alllogin_registration(userData,"candidate-provider-signin").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            if(resArray[2].category){
              this.storage.set("login_credential",result).then(()=>{
              //this.service.presentToast(resArray[1]);
              this.service.get_login_credential();
                this.router.navigateByUrl('/candidate/dashboard');
              });
            }else{
              this.editRegistration(resArray[2]);
            }    
          }else{
            this.service.Validalert(resArray[1]);
          }
        });  
            
      }
    );
  }

   public socialRegistration(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }/*  else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    } */
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        this.service.alllogin_registration(userData,"candidate-provider-registration").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){
            //this.service.present(resArray[1]);
            //window.location.reload();
            this.editRegistration(resArray[3]);
          }else{
            //this.dataservice.presentToast(resArray[0]);
            this.service.Validalert(resArray[1]);
          }
        });
            
      }
    );
  }

  forgot_password(){
    this.router.navigate(['/forgot-password',{type:'candidate'}]);
  }

  async editRegistration(cad){
      const modal = await this.modalCtrl.create({
        component: CandidateLoginModalpagePage,
        componentProps:{data:cad},
        cssClass:"candidate_modal",
        showBackdrop: true,
        backdropDismiss:false
      });
      /* modal.onDidDismiss().then(details => {
      if(details.data){
        this.dataserv.updateAddress(details.data);
        this._zone.run(() => {
          this.initialise();
        });
      }
    }); */
    return await modal.present();
  }

  ionViewWillLeave(){
    if(this.register){
      this.register.reset();
    }
    this.active_div=true;
  }

}
