import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  candidate_id:any;
  candetails:any;
  canqualification:any;
  dpimage:any;
  job_title:any;
  cv_url:any;
  experiencelist:any;
  languagelist:any;
  lookingforjoblist:any;
  skilllist:any;

  constructor(public acroute:ActivatedRoute,
    public dataservice: DataserviceService) { }

  ngOnInit() {
    this.candidate_id = this.acroute.snapshot.paramMap.get('can_id');
    console.log(this.candidate_id);
    this.getcandidate_details();
  }

  getcandidate_details(){
    let postdata = {
      "data":{
        "candadite_id":this.candidate_id
      }
     };
     console.log(postdata);
     //this.dataservice.loadercontinue();
     this.dataservice.can_details(postdata,"candidate-details").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.candidate_list = resArray[1];
        this.candetails=resArray[1];
        this.canqualification =resArray[2];
        this.dpimage =resArray[3];
        this.job_title=resArray[4];
        this.cv_url = resArray[5];
        this.experiencelist= resArray[6];
        this.languagelist = resArray[7];
        this.skilllist = resArray[8];
        this.lookingforjoblist = resArray[9];
      }else{
        this.dataservice.presentToast(resArray[0]);
      }
      //this.dataservice.hideLoader();
    });

  }

  download_resume(){
    window.open(this.cv_url);
  }

}
