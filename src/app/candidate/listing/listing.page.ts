import { Component, OnInit, ViewChild } from '@angular/core';
import {DataserviceService} from '../../Service/dataservice.service';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.page.html',
  styleUrls: ['./listing.page.scss'],
})
export class ListingPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  categorylist:any;
  category_id:any='';
  candidate_list=[];
  loc:any='';
  locationlist:any;
  errorMsg:any='';
  pages = []; ///pagination
  comDetlsShow = []; ///pagination
  hidden:boolean; ///pagination
  start:number=0;
  limit:number=10;
  totalData;

  constructor(public dataservice: DataserviceService,
    public router:Router,
    public alertController:AlertController) {
   }

  ngOnInit() {
    this.dataservice.get_method_apicall('candidate-category-list').then((result)=>{
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[0]){
        this.categorylist= resArray[1];
        this.locationlist = resArray[2];
        console.log(this.categorylist);
        this.category_id=this.categorylist[0].category_id;
        console.log(this.category_id);
        this.getcandidate_list(this.category_id);
        //this.getcandidate_list(this.category_id);  //first time api call for display candidate and category dynamic.
      }
    });
  }

  /* ionViewDidEnter() {
      this.categorylist.forEach(element => {
        if(this.categorylist.indexOf(element) == 0){
          console.log("llllllllllllll");
          document.getElementById(element.category_id).click();
        }
      });
  } */  

  getcategoryid(id){
    this.candidate_list = [];
    this.start=0;
    this.infiniteScroll.disabled = false;
    this.category_id=id;
    this.getcandidate_list(this.category_id);
  }

  getcandidate_list(id){
    let postdata = {
      "data":{
        "category_id":id,
        "job_location": this.loc?this.loc:'',
        "start":this.start,
        "limit":this.limit
      }
     };
     console.log(postdata);
     //this.dataservice.loadercontinue();
     this.dataservice.post_request_api_call(postdata,"candidate-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){
        this.errorMsg='';
        document.getElementById('vScroll').style.display = 'block';
        resArray[1].forEach(element => {
          this.candidate_list.push(element);
        });
        this.totalData = resArray[2];
        this.virtualScroll.checkEnd();
        if (this.start+this.limit >= this.totalData) {
          this.infiniteScroll.disabled = true;
        }
        // this.hidden = false; ///pagination
        // this.calculatePage(resArray[1].length); ///pagination
      }else{
        this.candidate_list = [];
        this.infiniteScroll.disabled = true;
        // this.comDetlsShow=[]; ///pagination
        // this.hidden = true; ///pagination
        // document.getElementById('showingResult').innerHTML = ""; ///pagination
        //this.dataservice.presentToast(resArray[1]);
        this.errorMsg=resArray[1];
        document.getElementById('vScroll').style.display = 'none';
      }
      //this.dataservice.hideLoader();
    });

  }

  loadData(event) {
    this.start = this.start+this.limit;
    setTimeout(() => {
    
      this.getcandidate_list(this.category_id);
      //Hide Infinite List Loader on Complete
      event.target.complete();

      //Rerender Virtual Scroll List After Adding New Data
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.start+this.limit >= this.totalData) {
        event.target.disabled = true;
      }
    }, 500);
  }

  ///pagination
  calculatePage(totalData){
    this.pages = [];
    let totalPages = Math.floor(totalData/10)+1;
    for(let i = 0; i < totalPages; i++){
      this.pages.push(i+1);
    }
    this.showPagination(1);
  }

  showPagination(n){
    this.comDetlsShow = [];
    document.getElementById('showingResult').innerHTML = "";
    let p = (Number(n)-1)*10;
    let q = Number(n)*10;
    if(q>this.candidate_list.length){
      q = this.candidate_list.length;
    }
    for(let i = p; i < q; i++){
      this.comDetlsShow.push(this.candidate_list[i]);
    }
    document.getElementById('showingResult').innerHTML ="Showing Result "+p+"-"+q+" of "+this.candidate_list.length;
    console.log(this.comDetlsShow); 
  }
///pagination

  getLocationJob(loc){
    let postData = {
      "data":{
        "job_category_id": 'this.activeCat',
        "job_location": loc
      }
    }
    this.dataservice.post_request_api_call(postData, 'api').then((res)=>{
      console.log(res);
    })
  }

  getlocation(loc,e){
    if(loc==this.loc){
      //console.log(loc +"unchecked");
      e.target.checked=false;
      this.loc='';
      this.getcandidate_list(this.category_id);
    }else{
      this.loc =loc;
      this.getcandidate_list(this.category_id);
    }  
  }

  viewprofile(id){
    this.router.navigate(['/admin/candidate-details',{can_id:id}]);
  }

  async deleteCandidate(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to delete this candidate?',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'CANCEL',
          cssClass: 'secondary',
          role:"cancel",
        },
        {
          text: 'YES',
          cssClass: 'secondary',
          handler: () => {
            let postdata = {
              "data":{
                "candidate_id":id,
              }
             };
             console.log(postdata);
             this.dataservice.post_request_api_call(postdata,"candidate-delete").then((result)=>{
              const resArray = Object.keys(result).map(i => result[i]);
              console.log(resArray);
              if(resArray[1]){ 
                this.dataservice.showalert(resArray[2], '');
                /* this.dataservice.get_method_apicall('candidate-category-list').then((result)=>{
                  const resArray = Object.keys(result).map(i => result[i]);
                  console.log(resArray);
                  if(resArray[0]){
                    this.categorylist= resArray[1];
                    this.locationlist = resArray[2];
                    console.log(this.category_id);
                    this.start = 0;
                    this.candidate_list = [];
                    this.infiniteScroll.disabled = false;
                    this.getcandidate_list(this.category_id);
                  }
                });    */           
              }else{
                this.dataservice.Validalert(resArray[2]);
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }

  addCandidate(){
    this.router.navigateByUrl('admin/add-candidate');
  }

}
