import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import { ModalController,NavParams} from '@ionic/angular';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-candidate-login-modalpage',
  templateUrl: './candidate-login-modalpage.page.html',
  styleUrls: ['./candidate-login-modalpage.page.scss'],
})
export class CandidateLoginModalpagePage implements OnInit {
  ccode:any;
  phone:any;
  category:any;
  categorylist:any;
  candidate_id:any;
  type:any;
  name;

  constructor(public service:DataserviceService,
    public modalctrl:ModalController,
    public navparam: NavParams,
    public router: Router,
    public storage: Storage
    ) {
      this.candidate_id=this.navparam.get('data').candidate_id;
  }

  ngOnInit() {
    this.name = this.navparam.get('data').name;
    this.service.get_method_apicall('category-list').then((result)=>{
      //console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]=="true"){
        this.categorylist= resArray[2];
        console.log(this.categorylist);
      }
      
    });
  }

  validateuser(register:NgForm){
    console.log(register);
    if (register.invalid || this.category=='Select Category') {
        //alert('invalid');
      return;
    }
    if(this.category!=0){
        let postdata = {
        "data":{"mob_country_code":this.ccode,
        "mobile":this.phone,
        "category_id":this.category,
        "candidate_id":this.candidate_id
        }
        }; 
          console.log(postdata);
          this.service.alllogin_registration(postdata,"provider-candidate-update").then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            console.log(resArray);
            if(resArray[0]=="true"){
              this.modalctrl.dismiss();
              this.storage.set("login_credential",result).then(()=>{
                this.service.Successalert(resArray[1]);
                this.service.get_login_credential();
                this.router.navigateByUrl('/candidate/dashboard');
              });
            }else{
              //this.service.presentToast(resArray[0]);
              //this.errorMsg = resArray[0];
            }
            // loading.dismiss();
          });
    }else{
      this.service.presentToast("Please select any category!");
    }      
  }

  closeModal(){
    this.modalctrl.dismiss();
  }

}
