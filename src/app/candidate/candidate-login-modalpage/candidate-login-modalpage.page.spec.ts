import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateLoginModalpagePage } from './candidate-login-modalpage.page';

describe('CandidateLoginModalpagePage', () => {
  let component: CandidateLoginModalpagePage;
  let fixture: ComponentFixture<CandidateLoginModalpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateLoginModalpagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateLoginModalpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
