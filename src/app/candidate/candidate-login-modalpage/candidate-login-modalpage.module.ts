import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CandidateLoginModalpagePage } from './candidate-login-modalpage.page';

const routes: Routes = [
  {
    path: '',
    component: CandidateLoginModalpagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CandidateLoginModalpagePage]
})
export class CandidateLoginModalpagePageModule {}
