import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {DataserviceService} from '../../Service/dataservice.service';
import {AlertController} from '@ionic/angular';
import { NgForm } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  public looking_edit:boolean=true;
  public workExp_edit:boolean=true;
  public quali_edit:boolean=true;
  public edu_edit:boolean=true;
  public lang_edit:boolean=true;
  personal_edit:boolean=true;
  password_edit:boolean=true;
  name:any;
  email:any;
  dpimage:any;
  rdrRslt:any;
  candidate_id:any;
  file_name:any;
  link:any;
  month =[{"name":"January","id":"1"},{"name":"February","id":"2"},
  {"name":"March","id":"3"},{"name":"April","id":"4"},
  {"name":"May","id":"5"},{"name":"June","id":"6"},
  {"name":"July","id":"7"},{"name":"August","id":"8"},
  {"name":"September","id":"9"},{"name":"October","id":"10"},
  {"name":"November","id":"11"},{"name":"December","id":"12"}
];
stmonth:any="Month";
styear:any="Year";
endmonth:any="Month";
endyear:any="Year";
selectableMonth = [];
yearlist=[];
endYearList = [];
endYearListTwo = [];
description:any;
checked:boolean=false;
company:any;
job_title:any;
experiencelist:any;
cand_experience_id:any='';

//qualification...
qstartyear:any="Year";
qendyear:any="Year";
qlevel = "Select Qualification Type";
college:any;
stream:any;
marks:any;
qualification_id:any='';
qualificatiolist:any;

//language...
fluency:any;
languagelist:any;
language:any;

//skill...
skil_name:any;
skilllist:any;

//looking for job..
category:any;
des_job_title:any;
job_type = "Select Job Type";
salary:any;
categorylist:any;
location:any;
lookingfor_id:any='';
lookingforjoblist:any;
//personal info..
//father_name:any;
fastName:any;
lastName:any;
address:any;
zipcode:any;
website:any;
can_phone:any;
alter_no:any;
personal_info_id:any='';
personalinfolist:any;
ye:any;
size:any;
profile_pic:any;
profileimg: boolean= false;
// health care
healthPart:boolean;
checkedAns = [];
hclno;
ohcl;
healthCareDetails;
healthcare_id:any = '';
DOH;DHA;MOH;
healthcareID;
/////////
countrylist;
gender = "Select Gender";
selected:any;
country = "Select Country";
statelist;
state = "Select State";
stat:any;
city;
//altmail;
nationlist;
Nationality = "Select Nationality";
nation:any;
c_code;
alt_c_code;
hcdEdit:boolean;
pinfoUpdate:boolean = false;
relatedJobs:any;
currencylist;
currency;
curren:any;
  canddetails: any;
  /////.....
  healthcountrylist: any;
  dha_liscence:any;
  dha_title:any;
  doh_liscence:any;
  doh_title:any;
  moh_liscence:any;
  moh_title:any;
  health_country:any;
  errmsg1:any;

  //change password..
  confpas:any;
  currentpass:any;
  newpass:any;
  errorMsgS:boolean=false;
  keyword: any;
  town: any;
  ///admin section
  candidateID;
  candidateCategory;

  constructor(private storage: Storage,
    public service: DataserviceService,
    private alertController:AlertController,
    private router: Router,
    public acRoute: ActivatedRoute
  ) { 
    
  }

  ngOnInit() {
    this.candidateID = this.acRoute.snapshot.paramMap.get('can_id');
    let currentyear=new Date().getFullYear();
    let start_year = currentyear-30;
    //console.log(start_year);
    for(let i=start_year; i<= currentyear;i++){
       this.yearlist.push(i);
    }
    //console.log(this.yearlist);
    this.service.getCountry().then((res)=>{
      //console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.countrylist=resArray[0];
      console.log(resArray);
    });
  
    this.service.getCountry_except_uae().then((res)=>{
      //console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.healthcountrylist=resArray[0];
      console.log(resArray);
    });

    this.service.getNationality().then((res)=>{
      //console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.nationlist=resArray[0];
      console.log(resArray);
    });

    this.service.getCurrency().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      this.currencylist=resArray[0];
      console.log(resArray);
    });
  }

  onChange(e){
    console.log(e);
    this.countrylist.forEach(element => {
      if(element.country==e){
        this.statelist = element.states;
        console.log(this.statelist)
      }
    });
  }

  lookingForEdit() {
    this.service.get_method_apicall('category-list').then((result)=>{
      //console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]=="true"){
        this.categorylist= resArray[2];
        console.log(this.categorylist);
      }  
    });

    console.log(this.lookingforjoblist);
    if(this.lookingforjoblist.length>0){
      console.log(this.lookingforjoblist.length);
      this.lookingfor_id=this.lookingforjoblist[0].candidate_looking_for_id;
      console.log(this.lookingfor_id);
      this.des_job_title=this.lookingforjoblist[0].desired_job_title;
      this.job_type=this.lookingforjoblist[0].job_type;
      this.currency =this.lookingforjoblist[0].currency;
      this.salary=this.lookingforjoblist[0].salary;
      this.category=this.lookingforjoblist[0].category;
      this.location=this.lookingforjoblist[0].location;
    }
    if(!this.lookingforjoblist[0].job_type){
      this.job_type = "Select Job Type";

    }
    if(!this.lookingforjoblist[0].currency){
      this.curren="Select Currency";
      this.currency="Select Currency";
    }
    console.log(this.lookingfor_id);
    this.looking_edit = false;
  }

  workExpEdit() {
    this.workExp_edit = false;
  }

  healthEdit(){
    this.healthPart = true;
    this.health_country = "Select Country";
  }

  qualiEdit() {
    //console.log(this.quali_edit)
    this.quali_edit = false;
    //this.qlevel = "Select Qualification Type";
      // this.qtype = "Select Qualification Type";
  }

  eduEdit() {
    this.edu_edit = false;
  }

  langEdit() {
    this.lang_edit = false;
  }

  jobDetails(id){
    this.router.navigate(['job-details',{id:id}]);
  }
  /* moreJobs(){
    this.router.navigateByUrl('/job-listing');
  } */

  ionViewWillEnter(){
    this.get_all_details();
  }

  get_all_details(){
    //get all details from storage..
    this.storage.get("login_credential").then((val)=>{
      if(val){
        const resArray = Object.keys(val).map(i => val[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          console.log(val);
          this.name=resArray[2].name;
          this.email=resArray[2].email;
          this.dpimage=resArray[2].provider_image;
          this.candidate_id=resArray[2].candidate_id;
          console.log(this.candidate_id);

          //candidate dashboard api calling..
          let postdata = {
            "data":{"candidate_id":this.candidateID?this.candidateID:this.candidate_id
            }
            };
          console.log(postdata);
          this.service.post_request_api_call(postdata,"candidate-dashboard").then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            console.log(resArray);
            if(resArray[1]){ 
              this.experiencelist= resArray[2];
              this.qualificatiolist=resArray[3];
              this.languagelist =resArray[4];
              this.skilllist=resArray[5];
              this.lookingforjoblist=resArray[6];
              this.candidateCategory=resArray[6][0].category;
              //console.log(this.candidateCategory);
              /* if(this.lookingforjoblist.length > 0){
                console.log('lllllll')
                document.getElementById('lookFor').innerHTML = 'Edit';
                console.log('lllllll')
              } */

              this.personalinfolist=resArray[7];
              if(this.personalinfolist.length > 0){
                console.log('lllllll')
                //document.getElementById('pInfo').innerHTML = 'Edit';
                this.pinfoUpdate = true;
                console.log('lllllll')
              }
              this.file_name = resArray[8];
              this.profile_pic = resArray[9];
              this.healthCareDetails = resArray[10];
              if(this.healthCareDetails.length > 0){
                this.healthcareID = resArray[10][0].candidate_medical_info_id;
                this.hcdEdit = true;
              }
              //this.canddetails = resArray[11];
              console.log(this.healthCareDetails);
            this.service.post_request_api_call(postdata,"candidate-related-job").then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            console.log(resArray);
            if(resArray[1]){
              this.relatedJobs = resArray[2];
            }
          });

            }else{
              this.service.Validalert(resArray[0]);
            }
            //this.service.hideLoader();
          });
        }
      }else{
        this.router.navigateByUrl('/home');
      }  
    });

  }

  ionViewDidEnter(){
    
  }

  setEndYear(){
    this.endYearList = [];
    if(Number(this.qstartyear) >= new Date().getFullYear()-2){
      let currentyear=new Date().getFullYear()+5;
      let start_year = Number(this.qstartyear)+1;
      for(let i=start_year; i<= currentyear;i++){
        this.endYearList.push(i);
      }
    }else{
      let currentyear=new Date().getFullYear();
      let start_year = Number(this.qstartyear)+1;
      for(let i=start_year; i<= currentyear;i++){
        this.endYearList.push(i);
      }
    }
  }

  setEndYearTwo(){
    this.endYearListTwo = [];
    this.yearlist.forEach(element => {
      if(element >= this.styear){
        this.endYearListTwo.push(element);
      }
    });
  }

  setEndMonth(){
    this.selectableMonth = [];
    let xm;
    if(this.endyear == this.styear){
      this.month.forEach(element => {
        if(element.name == this.stmonth){
          xm = element.id;
        }
      });
      this.month.forEach(element => {
        console.log(this.month)
        if(Number(element.id) >= Number(xm)){
          this.selectableMonth.push(element);
        }
      });
    }else if(this.endyear != 'Year'){
      console.log(this.month)
      this.selectableMonth = this.month;
    }
  }

  uploadIMG(){
    document.getElementById('videoInput').click();
  }

  uploadPrfleIMG(){
    document.getElementById('picInput').click();
  }
  

  changeprofilePic(ev) {
    let file = ev.files[0];
    if(file.type=="image/jpeg" || file.type=="image/png"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result);
        this.profile_pic = reader.result;
        // this.submitData(reader.result);
        this.profileimg= true;

      }
      reader.readAsDataURL(file);
    }else{
      this.service.Validalert("Only .jpg and .png file type are allowed.");
      ev.value='';
    }  

  }

  updatePic(){
    let postdata = {
      "data":{
        "candidate_id":this.candidateID?this.candidateID:this.candidate_id,
        "profile_pic":this.profile_pic
      }
      }; 
      console.log(postdata);
      this.service.loadercontinue();
      this.service.post_request_api_call(postdata,"candidate-profile-pic-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]=="true"){
        this.service.userpic(resArray[2]);
        window.location.reload();
        // this.profile_pic= this.service.mainurl+resArray[2];
        console.log(this.profile_pic);
        this.profileimg= false;
        //this.service.presentToast('Profile picture updated successfully!')
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });
  }

  changeImg(ev) {
    let file = ev.files[0];
    this.file_name=file.name;
    let extension=this.file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
    if(file.type=="application/pdf"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result);
        this.rdrRslt = reader.result;
        // this.submitData(reader.result);
        this.uploadcv(this.rdrRslt,extension);
      }
      reader.readAsDataURL(file);
    }else{
      this.service.Validalert("Please select only .pdf extension document.")
      this.file_name="";
      ev.value='';
    }  
  }

  uploadcv(cv,ext){
    let postdata = {
      "data":{"candidate_id":this.candidateID?this.candidateID:this.candidate_id,
      "candidate_cv":cv,
      "cv_extension": ext
      }
      }; 
      console.log(postdata);
      this.service.loadercontinue();
    this.service.post_request_api_call(postdata,"candidate-cv-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]=="true"){ 
        //this.service.presentToast(resArray[1]);
        this.file_name = resArray[2];
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });
  }

  view_yourcv(){
    window.open(this.file_name);
  }
  
  validateuser(workexp: NgForm){
    if (workexp.invalid || this.stmonth == 'Month' || this.styear == 'Year' ) {
      if(!this.checked){
        if(this.endmonth == 'Month' || this.endyear == 'Year'){
          return;
        }
      }else{
        return;
      }
      //alert('invalid');
    
  }
    this.save_experience();
  }

  save_experience(){
    //console.log(this.stmonth + this.styear);
    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "job_title": this.job_title,
        "company": this.company,
        "from_month": this.stmonth,
        "from_year": this.styear,
       "to_month": this.endmonth!="Month"?this.endmonth:'',
       "to_year": this.endyear!="Year"?this.endyear:'',
       "description": this.description,
       "terms": this.checked,
        "experience_id":this.cand_experience_id?this.cand_experience_id:''
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-work-experience-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          //window.location.reload();
          this.workExp_edit=true;
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#work";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[1]);
      }
      this.service.hideLoader();
    });

  }

  setExpChecked(){
    if(this.checked){
      this.endyear = "Year";
      this.endmonth ="Month";
      console.log("lllllllllllll");
    }else{
      this.setEndYearTwo();
      this.setEndMonth();
    }
  }

  edit_experience_idwise(exp){
    this.workExp_edit=false;
    console.log(exp);
    this.cand_experience_id=exp.cand_experience_details_id;
    console.log(this.cand_experience_id);
    this.job_title=exp.job_title;
    this.company=exp.company;
    this.stmonth=exp.from_month;
    this.styear=exp.from_year;
    this.description=exp.description;
    if(exp.is_current_employer=='1'){
      this.checked=true;
      this.endyear="Year";
      this.endmonth="Month";
    }else{
      this.checked=false;
      this.setEndYearTwo();
      this.endyear=exp.to_year;
      this.setEndMonth();
      this.endmonth=exp.to_month;
    }
    
  }

  delete_experience(){
    let postdata = {
      "data":{
        "experience_id":this.cand_experience_id
      }
     };
     console.log(postdata);
     //this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-work-experience-delete").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          window.location.reload();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      //this.service.hideLoader();
    });

  }

  cancel_experience(workexp: NgForm){
    workexp.reset();
    this.stmonth="Month";
    this.styear="Year";
    this.endmonth="Month";
    this.endyear="Year";
    this.cand_experience_id='';
    console.log(this.cand_experience_id);
    this.workExp_edit = true; 
    /* setTimeout(() => {
      window.location.reload();
    }, 1000); */
  }
  
  validate_qualification(qualification:NgForm){
    if (qualification.invalid || this.qstartyear == 'Year' || this.qendyear=='Year' || this.qlevel == 'Select Qualification Type') {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "qualification_type": this.qlevel,
        "school_college": this.college,
        "from_year": this.qstartyear,
        "to_year": this.qendyear,
        //"specialization": this.stream,
        "marks_grade": this.marks,
        "qualification_id":this.qualification_id
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-qualification-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.quali_edit=true;
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#qualification";
          }
          qualification.onReset();
          this.qstartyear="Year";
          this.qendyear="Year";
          this.qlevel="Select Qualification Type";
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });

  }

  edit_qualification(q){
    this.quali_edit=false;
    this.qlevel= q.qualification_type;
    this.college= q.school_college;
    this.qstartyear= q.from_year;
    this.setEndYear();
    this.qendyear= q.to_year;
    //this.stream = q.specialization;
    this.marks= q.marks_grade;
    this.qualification_id= q.candidate_qualification_id;
  }

  cancel_qualification(qualification: NgForm){
    qualification.reset();
    this.qstartyear="Year";
    this.qendyear="Year";
    this.qlevel="Select Qualification Type";
    this.qualification_id='';
    this.quali_edit = true; 
    /* setTimeout(() => {
      window.location.reload();
    }, 1000); */

  }

  delete_qualification(){
    let postdata = {
      "data":{
        "qualification_id":this.qualification_id
      }
     };
     console.log(postdata);
     //this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-qualification-delete").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          window.location.reload();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      //this.service.hideLoader();
    });

  }

  validate_language(languageform: NgForm){
    if (languageform.invalid) {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "language": this.language,
        "fluency": this.fluency
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-language-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.lang_edit = true;
          this.language="";
          this.fluency="";
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#language";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });
  }

  cancel_language(){
    this.lang_edit = true;
  }

  cancel_lookingfor(){
    this.looking_edit = true;
  }

  cancel_skill(){
    this.edu_edit =true;
  }

  cancel_health(h: NgForm){
    h.reset();
    this.healthcare_id = '';
    this.healthPart = false;

  }

  checking(ev){
    if(ev.target.checked){
      //console.log(ev.target.value)
      /* this.checkedAns.push(ev.target.value)
      console.log(this.checkedAns) */    
    }else{
      if(ev.target.value=="DOH"){
        this.doh_liscence="";
        this.doh_title="";
      }else if(ev.target.value=="DHA"){
        this.dha_liscence="";
        this.dha_title="";
      }else if(ev.target.value=="MOH"){
        this.moh_liscence="";
        this.moh_title="";
      }else{
        this.doh_liscence="";
        this.doh_title="";
        this.dha_liscence="";
        this.dha_title="";
        this.moh_liscence="";
        this.moh_title="";
      }
      /* let indexNo = this.checkedAns.indexOf(ev.target.value);
      this.checkedAns.splice(indexNo, 1);
      console.log(this.checkedAns) */
    }
  }

  validatehealth(health: NgForm){
    if (health.invalid ) {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.service.userid,
        "doh": this.DOH,
        "doh_license_no": this.doh_liscence,
        "doh_license_title": this.doh_title,
        "dha": this.DHA,
        "dha_license_no": this.dha_liscence,
        "dha_license_title": this.dha_title,
        "moh": this.MOH,
        "moh_license_no": this.moh_liscence,
        "moh_license_title": this.moh_title,
        "other_country_name":this.health_country!="Select Country"?this.health_country:'',
        "medical_info_id": this.healthcareID?this.healthcareID:null
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-medical-info-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.healthPart = false;
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#health";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[1]);
      }
      this.service.hideLoader();
    });

  }

  clickEdit(){
    document.getElementById('editIcon').click();
  }

  edit_heatth_idwise(h){
    /* this.checkedAns = [];
    this.checkedAns = h.healthcare_license.split(',');
    this.checkedAns.forEach(element => {
      if(element == 'DOH'){this.DOH = true;}
      if(element == 'DHA'){this.DHA = true;}
      if(element == 'MOH'){this.MOH = true;}
    });
    this.healthPart = true;
    this.hclno = h.healthcare_license_no;
    this.ohcl = h.other_healthcare_license;
    this.healthcare_id = h.candidate_medical_info_id; */
    console.log(h);
    this.healthPart = true;
    if(h.doh=='1'){
      this.DOH = true;
      this.doh_liscence=h.doh_license_no;
      this.doh_title=h.doh_license_title;
    }
    if(h.dha=='1'){
      this.DHA = true;
      this.dha_liscence=h.dha_license_no;
      this.dha_title=h.dha_license_title;
    }
    if(h.moh=='1'){
      this.MOH = true;
      this.moh_liscence=h.moh_license_no;
      this.moh_title=h.moh_license_title;
    }
    if(h.other_country_name){
      this.health_country=h.other_country_name;
    }else{
      this.health_country="Select Country";
    }
  }

  validate_education(education: NgForm){
    if (education.invalid) {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "skill": this.skil_name
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-skill-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.edu_edit =true;
          this.skil_name= "";
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#skill";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });

  }

  delete_skill(id){
    this.presentAlertConfirm(id,'skill');

  }

  delete_language(id){
    this.presentAlertConfirm(id,'language');
  }

  async presentAlertConfirm(id,s) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log(id);
            if(s=='skill'){
              this.deleteskill_idwise(id);
            }else{
              this.deletelanguage_idwise(id);
            }  
          }
        }
      ]
    });

    await alert.present();
  }

  deleteskill_idwise(id){
    let postdata = {
      "data":{
        "skill_id":id
      }
     };
     console.log(postdata);
     //this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-skill-delete").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        /* setTimeout(() => {
          window.location.reload();
        }, 2000); */
        this.get_all_details();
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      //this.service.hideLoader();
    });

  }

  deletelanguage_idwise(id){
    let postdata = {
      "data":{
        "language_id":id
      }
     };
     console.log(postdata);
     //this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-language-delete").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        /* setTimeout(() => {
          window.location.reload();
        }, 2000); */
        this.get_all_details();
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      //this.service.hideLoader();
    });

  }

  validate_job(job: NgForm){

    if (job.invalid || this.job_type == "Select Job Type" || !this.currency) {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "desired_job_title": this.des_job_title,
        "job_type": this.job_type,
        "salary": this.salary,
        "category": this.category,
        "location": this.location,
        "looking_for_id": this.lookingfor_id,
        "currency": this.currency
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-looking-for-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.looking_edit =true;
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#lookingFor";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });

  }

  persolnalEdit(){
    if(this.personalinfolist.length>0){
      console.log(this.personalinfolist.length);
      this.fastName=this.personalinfolist[0].first_name;
      this.lastName=this.personalinfolist[0].last_name;
      this.address=this.personalinfolist[0].address;
      this.zipcode=this.personalinfolist[0].zip_code;
      this.can_phone=this.personalinfolist[0].mobile;
      this.website=this.personalinfolist[0].website;
      this.personal_info_id=this.personalinfolist[0].cand_address_info_id;
      this.country = this.personalinfolist[0].country?this.personalinfolist[0].country:'Select Country';
      this.onChange(this.personalinfolist[0].country);
      this.state = this.personalinfolist[0].state?this.personalinfolist[0].state:'Select State';
      this.city = this.personalinfolist[0].city;
      this.Nationality = this.personalinfolist[0].nationality?this.personalinfolist[0].nationality:'Select Nationality';
      this.gender = this.personalinfolist[0].gender?this.personalinfolist[0].gender:'Select Gender';
      //this.altmail = this.personalinfolist[0].alternate_email_id;
      this.c_code = this.personalinfolist[0].mob_country_code;
      //this.alt_c_code = this.personalinfolist[0].alt_no_country_code;
      console.log(this.personal_info_id);
    }
    this.personal_edit=false;
  }

  cancel_personalInfo(){
    this.personal_edit =true;
  }

  validate_personalinfo(pinfo : NgForm){
    if (pinfo.invalid || this.gender == "Select Gender" || this.country == "Select Country" || this.state == "Select State" || this.Nationality == "Select Nationality") {
      //alert('invalid');
      return;
    }

    let postdata = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.candidate_id,
        "address": this.address,
        "zip_code": this.zipcode,
        "phone_no": this.can_phone,
        "website": this.website,
        "address_info_id": this.personal_info_id,
        "city": this.city,
        "state": this.state,
        "country": this.country,
        "phn_no_country_code": this.c_code,
        "gender": this.gender,
        "nationality": this.Nationality,
        "first_name": this.fastName,
        "last_name": this.lastName
      }
     };
     console.log(postdata);
     this.service.loadercontinue();
     this.service.post_request_api_call(postdata,"candidate-address-info-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]){ 
        //this.service.presentToast(resArray[1]);
        setTimeout(() => {
          this.personal_edit=true;
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#personalInfo";
          }
          this.get_all_details();
        }, 2000);
        
      }else{
        this.service.Validalert(resArray[0]);
      }
      this.service.hideLoader();
    });
  }

  validate_password(pass:NgForm){
    console.log(pass);
    if (pass.invalid) {
       // alert('invalid');
      return;
    }

    let postData = {
      "data":{
        "candidate_id": this.candidateID?this.candidateID:this.service.userid,
        "current_password": this.currentpass,
        "new_password": this.newpass 
      }
     };
    console.log(postData);
    this.service.loadercontinue();
    this.service.post_request_api_call(postData, 'candidate-password-update').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      console.log(resArray);
      if(resArray[1]){
        //this.service.showalert(resArray[2],'')
        setTimeout(() => {
          this.password_edit=true;
          this.personal_edit=true;
          this.currentpass="";
          this.newpass ="";
          this.confpas ="";
          if(!this.candidateID){
            window.location.href="/candidate/dashboard#personalInfo";
          }
          this.get_all_details();
        }, 2000);
      }else{
        this.service.Validalert(resArray[2]);
      }
      this.service.hideLoader();
    });

  }

  focusOut(){
    if(this.newpass!=this.confpas){
      this.errorMsgS = true;
    }else{
      this.errorMsgS = false;
    }  

  }

  changepassword(){
    this.password_edit=false;
  }

  cancel_password(){
    this.password_edit=true;
    this.currentpass="";
    this.newpass ="";
    this.confpas ="";
  }

  search(){
    if(this.keyword || this.town){
      this.router.navigate(['/job-search',{text:this.keyword?this.keyword:'',town:this.town?this.town:''}]);
    }else{
      this.service.Validalert("Please enter keyword(skill) or place(town) or both.")
    }   
  }

}
