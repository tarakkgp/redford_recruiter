import { Component, OnInit, ViewChild } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-applied-joblist',
  templateUrl: './applied-joblist.page.html',
  styleUrls: ['./applied-joblist.page.scss'],
})
export class AppliedJoblistPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  job_details_by_id=[];
  count: any;
  start:number=0;
  limit:number=10;
  totaldata;

  constructor(public dataService: DataserviceService,
    public storage: Storage,
    public router: Router) { }

  ngOnInit() {
    this.storage.get("login_credential").then((val)=>{
      if(val){
        this.getfirst_joblist();
      }
    });
  }

  ionViewWillLeave(){
    this.start=0;
    this.job_details_by_id = [];
    this.infiniteScroll.disabled = false;
  }

getfirst_joblist(){
  let postData = {
    "data":{
      "candidate_id":this.dataService.userid?this.dataService.userid:'',
      "start":this.start,
      "limit":this.limit
    }
  };
  console.log(postData);
  this.dataService.jobsListing('candidate-applied-job-listing', postData).then((result)=>{
    const resArray = Object.keys(result).map(i => result[i]);
    console.log(resArray);
    if(resArray[1]){
      resArray[2].forEach(element => {
        this.job_details_by_id.push(element);
      });
      this.virtualScroll.checkEnd();
      this.totaldata = resArray[3];
      if (this.start+this.limit >= this.totaldata) {
        this.infiniteScroll.disabled = true;
      }
    }else{
      //this.dataService.Validalert(resArray[2]);
      this.start=0;
      this.totaldata=0;
      this.job_details_by_id = [];
    }
  });
}

loadData(event) {
  this.start = this.start+this.limit;
  setTimeout(() => {
  
    this.getfirst_joblist();
    //Hide Infinite List Loader on Complete
    event.target.complete();

    //Rerender Virtual Scroll List After Adding New Data

    // App logic to determine if all data is loaded
    // and disable the infinite scroll
    if (this.start+this.limit >= this.totaldata) {
      event.target.disabled = true;
    }
  }, 500);
}

job_details(id){
  this.router.navigate(['job-details',{id:id}]);
}
}
