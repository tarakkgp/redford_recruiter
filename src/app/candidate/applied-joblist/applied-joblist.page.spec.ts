import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedJoblistPage } from './applied-joblist.page';

describe('AppliedJoblistPage', () => {
  let component: AppliedJoblistPage;
  let fixture: ComponentFixture<AppliedJoblistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedJoblistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedJoblistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
