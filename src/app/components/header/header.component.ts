import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import {Storage} from '@ionic/storage'
import {DataserviceService} from '../../Service/dataservice.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  firstName:any;
  profilePic:any;
  lastName:any;
  
  constructor(public router:Router,
    private storage: Storage,
    public dataserv: DataserviceService) { }

  ngOnInit() {
    /* this.storage.get("login_credential").then((val)=>{
      if(val){   
        console.log(this.dataserv.login_type);
        console.log(this.dataserv.userid);
        console.log(this.dataserv.user_pic);
      }
    });   */  

    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{
            "user_id":this.dataserv.userid,
            "user_type":this.dataserv.login_type
          }
        };    
        this.dataserv.post_request_api_call(postdata,'header-credential-details').then((result)=>{
          console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.firstName=resArray[2].first_name;
            this.profilePic=resArray[2].profile_picture;
            this.lastName =resArray[2].last_name;
          }

        });
      }
    });
    
  } 

  home(){
    this.router.navigateByUrl('/home');
  }

  jsSignin(){
    this.router.navigate(['/candidate/login',{type:"login"}]);
  }

  jsDashboard(){
    this.router.navigateByUrl('/candidate/dashboard');
  }

  jsDetails(){
    this.router.navigateByUrl('/candidate/details');
  }

  jsListing(){
    this.router.navigateByUrl('/candidate/listing');
  }

  clogin(){
    this.router.navigateByUrl('/client/login');
  }

  jobListing(){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        this.router.navigate(['/job-listing',{id:this.dataserv.candidate_cat_id}]);
      }else{
        this.router.navigateByUrl('/job-listing');
      }
    });    
  }

  jobPost() {
    this.router.navigateByUrl('/post-job');
  }

  logout(){
    this.storage.remove('login_credential').then(()=>{
      this.dataserv.get_login_credential();
      this.router.navigateByUrl('/home');
    });
  }
  
  clientDashboard(){
    this.router.navigateByUrl('/client/dashboard');
  }

  jobposting(){
    this.router.navigateByUrl('/client/job-post');
  }

  registercv(){
    if(!this.dataserv.userid){
      this.router.navigate(['/candidate/login',{type:"register"}]);
    }
  }

  post_job(){
    this.router.navigate(['/client/login',{redirect:"company"}]);
  }

  clientCompany(){
    this.router.navigateByUrl('/client/company-list');
  }

  goto_profile(){
    if(this.dataserv.login_type == 'client'){
      this.router.navigateByUrl("/client/profile");
    }
  }

  appledjob(){
    this.router.navigateByUrl("/candidate/applied-joblist");
  }

  FvouriteJob(){
    this.router.navigateByUrl("/candidate/favourite-joblist");
  }

}
