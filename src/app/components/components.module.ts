import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ClientsHeaderComponent } from './clients-header/clients-header.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {FormsModule} from '@angular/forms'




@NgModule({
  declarations: [FooterComponent,HeaderComponent,AdminHeaderComponent,ClientsHeaderComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  
  exports: [FooterComponent,HeaderComponent,AdminHeaderComponent,ClientsHeaderComponent]
})
export class ComponentsModule { }
