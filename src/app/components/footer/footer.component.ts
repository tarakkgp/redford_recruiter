import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {DataserviceService} from '../../Service/dataservice.service';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  semail;
  msg:any="";
  cmsfooterList:any;
  title;
  content;
  y = new Date().getFullYear();

  constructor(public dataserv: DataserviceService,
    public router: Router,
    public storage: Storage
    ) { }

  ngOnInit() {
    this.dataserv.post_request_api_call("",'home-cms-list').then((result)=>{
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.cmsfooterList=resArray[2];
      }
    });

    let postdata = {
      "data":{
        "user_type": ''
      }
    }; 
    console.log(postdata);
    this.dataserv.post_request_api_call(postdata,"who-we-are-details").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.title = resArray[2].title;
        this.content = resArray[2].content;
      }else{
        this.title = '';
        this.content = '';
      }
    });
  }

  subscribeMail(){
    
    /* let postdata = {
      "data":{
        //"job_id":this.acRoute.snapshot.paramMap.get('id'),
        "candidate_id":"7"
      }
     }; */
    //this.service.post_request_api_call(postdata,'job-details').then((result)=>{});
  }

  postJobs(){
    console.log('this.email')
  }

  async validatelogin(form: NgForm){
    console.log(form);
    if (form.invalid) {
        //alert('invalid');
      return;
    }
    if(this.semail){
      let postdata = {
        "data":{
          "email":this.semail
        }
      }
      console.log(postdata);
      this.dataserv.post_request_api_call(postdata,'subscribed').then((result)=>{
        //console.log(result)
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.msg =resArray[2];
          form.onReset();
          setTimeout(() => {
            this.msg="";
          },3000);  
        }else{
          form.onReset();
          this.msg =resArray[2];
          setTimeout(() => {
            this.msg="";
          },3000);
        }
    });
    }

  }

  shareFacebook(e) {
    console.log(window.location.href);
    let url = window.location.href.toString();
    console.log(url);
    e.preventDefault();
    var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + 'http://redford.bluehorse.co', 'facebook-popup', 'height=350,width=600');
    if(facebookWindow.focus) { facebookWindow.focus(); }
    return false;
  }

  shareTwitter(e) {
    e.preventDefault();
    var twitterWindow = window.open('https://twitter.com/share?url=' + 'http://redford.bluehorse.co', 'twitter-popup', 'height=350,width=600');
    if(twitterWindow.focus) { twitterWindow.focus(); }
    return false;
  }

  /* shareGooglePlus(e) {
    //  var _this = this;
    e.preventDefault();
    var GooglePlusWindow = window.open('https://plus.google.com/share?url=' + 'http://redford.bluehorse.co', 'googleplus-popup', 'height=350,width=600');
    if(GooglePlusWindow.focus) { GooglePlusWindow.focus(); }
    return false;
  } */

  shareLinkedIn(e) {
  var url =  'http://redford.bluehorse.co';
  var title = 'Redford' //this.contributionDetails.Title;
  var text = document.URL;
  window.open('http://www.linkedin.com/shareArticle?mini=true&url='+url+'&title='+title, 'sharer', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
  }

  Gotocmspagelink(link){
    this.router.navigate(['/cms',{q:link}]);
  }

  jobSearch(){
    this.router.navigateByUrl('/job-search');
  }

  jobListing(){
    this.router.navigate(['/job-listing',{id:this.dataserv.candidate_cat_id}]);
    // this.storage.get('login_credential').then((val)=>{
    //   if(val){
    //     this.router.navigate(['/job-listing',{id:this.dataserv.candidate_cat_id}]);
    //   }else{
    //     this.router.navigateByUrl('/job-listing');
    //   }
    // });
  }

  regCV(){
    this.router.navigateByUrl('/candidate/dashboard');
    // this.storage.get('login_credential').then((val)=>{
    //   if(val){
    //     this.router.navigateByUrl('/candidate/dashboard');
    //   }else{
    //     this.router.navigate(['/candidate/login',{type:"register"}]);
    //   }
    // });
  }

  canDashboard(){
    this.router.navigateByUrl('/candidate/dashboard');
  }

  canAppliedjob(){
    this.router.navigateByUrl("/candidate/applied-joblist");
  }

  canFevJob(){
    this.router.navigateByUrl("/candidate/favourite-joblist");
  }

  footerDashboard(){
    this.router.navigateByUrl('/client/dashboard');
    // this.storage.get('login_credential').then((val)=>{
    //   if(val){
    //     this.router.navigateByUrl('/client/dashboard');
    //   }else{
    //     this.router.navigate(['/client/login',{type:"register"}]);
    //   }
    // });
  }

  footerProfile(){
    this.router.navigateByUrl("/client/profile");
  }

  footerCompany(){
    this.router.navigateByUrl('/client/company-list');
    // this.storage.get('login_credential').then((val)=>{
    //   if(val){
    //     this.router.navigateByUrl('/client/company-list');
    //   }else{
    //     this.router.navigate(['/client/login',{type:"register"}]);
    //   }
    // });
  }
  
  footerJobpost(){
    this.router.navigateByUrl('/client/job-post');
    // this.storage.get('login_credential').then((val)=>{
    //   if(val){
    //     this.router.navigateByUrl('/client/job-post');
    //   }else{
    //     this.router.navigate(['/client/login',{type:"register"}]);
    //   }
    // });
  }

}
