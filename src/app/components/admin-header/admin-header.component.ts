import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { DataserviceService } from '../../Service/dataservice.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss'],
})
export class AdminHeaderComponent implements OnInit {

  adminName;
  adminProfilePic;

  constructor(
    public storage: Storage,
    public router: Router,
    public service: DataserviceService
  ) { }

  ngOnInit() {
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{
            "user_id":this.service.userid,
            "user_type":this.service.login_type
          }
        };    
        this.service.post_request_api_call(postdata,'header-credential-details').then((result)=>{
          console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.adminName=resArray[2].name;
            this.adminProfilePic=resArray[2].profile_picture;
            //this.lastName =resArray[2].last_name;
          }
        });
      }else{
        this.router.navigateByUrl('/admin/login');
      }
    });
  }

  changeLogo(){
    this.router.navigateByUrl('/admin/change-logo');
  }

  adminDashboard(){
    this.router.navigateByUrl('/admin/dashboard');
  }

  adminClient(){
    this.router.navigateByUrl('/admin/clients');
  }

  adminCompany(){
    this.router.navigate(['/admin/company-list', {admin:true}]);
  }

  adminCandidate(){
    this.router.navigateByUrl('/admin/candidates');
  }

  adminProfile(){
    this.router.navigateByUrl('/admin/profile');
  }

  adminLogout(){
    this.storage.remove('login_credential').then(()=>{
      this.router.navigateByUrl('/admin/login');
    });
  }

  goto_jos(){
    this.router.navigateByUrl('/admin/job-opening-status');
  }

  goto_cjs(){
    this.router.navigateByUrl('/admin/candidate-job-status');
  }

  gotocms(){
    this.router.navigateByUrl('/admin/cms-list');
  }

  goto_testimonial(){
    this.router.navigateByUrl('/admin/testimonial-listing');
  }

  emailTemplate(){
    this.router.navigateByUrl('/admin/email-template');
  }

  whoweare(){
    this.router.navigateByUrl('/whoweare');
  }

}
