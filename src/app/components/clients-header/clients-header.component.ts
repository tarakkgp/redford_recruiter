import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';


@Component({
  selector: 'app-clients-header',
  templateUrl: './clients-header.component.html',
  styleUrls: ['./clients-header.component.scss'],
})
export class ClientsHeaderComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {}

  home(){
    this.router.navigateByUrl('/home');
  }

  jobListing(){
    this.router.navigateByUrl('/job-listing');
  }

  jobPost() {
    this.router.navigateByUrl('/job-post');
  }

}
