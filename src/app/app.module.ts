import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
// import { ComponentsModule } from './components/components.module';
// import { FooterComponent } from './components/footer/footer.component';
// import { HeaderComponent } from './components/header/header.component';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService,LinkedinLoginProvider } from 'angular-6-social-login';  
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login'; 
import {CandidateLoginModalpagePageModule} from '../app/candidate/candidate-login-modalpage/candidate-login-modalpage.module'; 
import {LoginModalpagePageModule} from '../app/clients/login-modalpage/login-modalpage.module';
import {EditShortlistedCandidatePageModule} from '../app/clients/edit-shortlisted-candidate/edit-shortlisted-candidate.module';
//import { MdToHtmlPipe } from './pipe/md-to-html.pipe';presentPopover
import {StatusPopoverPageModule} from '../app/status-popover/status-popover.module';


export function socialConfigs() {  
  const config = new AuthServiceConfig(  
    [  
      {  
        id: FacebookLoginProvider.PROVIDER_ID,  
        provider: new FacebookLoginProvider('516581289192827')  
      },  
      {  
        id: GoogleLoginProvider.PROVIDER_ID,  
        provider: new GoogleLoginProvider('556358369890-sgguqmiriftaa512dvlcrf4te44m3qse.apps.googleusercontent.com')  
      },
      /* {
        id: LinkedinLoginProvider.PROVIDER_ID,
        provider: new LinkedinLoginProvider('81c7cdjben59eg')
      } */ 
    ]  
  );  
  return config;  
}  


@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule,
     IonicModule.forRoot(),
      AppRoutingModule,
      HttpClientModule,
      IonicStorageModule.forRoot(),
      SocialLoginModule,
      CandidateLoginModalpagePageModule,
      LoginModalpagePageModule,
      EditShortlistedCandidatePageModule,
      StatusPopoverPageModule
    ],
  providers: [
    AuthService,  
    {  
      provide: AuthServiceConfig,  
      useFactory: socialConfigs  
    },
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
