import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {ToastController,LoadingController,AlertController} from '@ionic/angular'
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';

//var domain = "https://www.redfordrecruiters.com:3302";
var domain = "http://172.105.47.7:3302";

let apiUrl = domain+"/api/v1/";

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  mainurl = domain;
  homecategoryimg_link = domain+"/contents/categories/";
  
  name:any;
  email:any;
  userid:any;
  login_type:any;
  user_first_name:any;
  user_pic:any;
  candidate_cat_id;
  countryurl = "assets/data/country_state.json";
  nationalityurl = "assets/data/nationality.json";
  currencyurl = "assets/data/currency.json";
  exceptuaecountryurl = "assets/data/countries.json";
  provider:any;

  mainLogo;

  constructor(private http: HttpClient,
    private toastCtrl: ToastController,
    private storage: Storage,
    private loadingCtrl:LoadingController,
    public router: Router,
    public alertController:AlertController
  ) { 
     this.get_login_credential();
  }

  async userpic(p){
    this.user_pic = p;
  }

  async get_login_credential(){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        // this.router.navigateByUrl('/home');
        const resArray = Object.keys(val).map(i => val[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          console.log(val);
          this.name=resArray[2].name;
          this.email=resArray[2].email;
          this.provider= resArray[2].provider;
          this.login_type = resArray[2].type;
          if(resArray[2].provider==null){
            //console.log("provider null");
            this.user_first_name=resArray[2].first_name;
          }else{
            this.user_first_name=resArray[2].name;
          }

          //console.log(resArray[2].profile_picture);
          if(resArray[2].profile_picture != null){
            this.user_pic = domain + "/"+resArray[2].profile_picture;
          } else {
            if(resArray[2].provider_image != null){
              this.user_pic = domain +resArray[2].provider_image;
            } else {
              this.user_pic = domain + "/contents/no_image_redford.jpg";
            }
          }


          // if(resArray[2].profile_picture == ''){
          //   if(resArray[2].provider_image!=null){
          //     this.user_pic = "https://www.redfordrecruiters.com:3302/"+resArray[2].provider_image;
          //     console.log("User_pic" + this.user_pic);
          //   }else{
          //     //this.user_pic = "https://www.redfordrecruiters.com:3302/"+resArray[2].provider_image;
          //     this.user_pic = "https://www.redfordrecruiters.com:3302/contents/no_image_redford.jpg";
          //     console.log("User_pic" + this.user_pic);
          //   }
          // }else{
          //   this.user_pic = "https://www.redfordrecruiters.com:3302/"+resArray[2].profile_picture;
          //   //this.user_pic = "https://www.redfordrecruiters.com:3302/contents/no_image_redford.jpg";
            
          //   console.log("User_pic" + this.user_pic);
          // }
          
          if(this.login_type == 'client'){
            this.userid=resArray[2].client_id;
          }else if(this.login_type == 'candidate'){
            this.userid=resArray[2].candidate_id;
            this.candidate_cat_id = resArray[2].category;
          }else{
            this.userid=resArray[2].admin_id;
          }
          console.log(this.login_type);
        }
      }else{
        //this.router.navigateByUrl('/');
        this.name='';
        this.email='';
        this.userid='';
        this.login_type='';
        this.provider='';
        console.log("llllllll");
      } 
    });

  }



  async jobsListing(type, postData){
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type, postData).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }
  
  async alllogin_registration(credentials, type) {
    console.log("aaaaaaaa");
    console.log(credentials);
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type, credentials).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          //console.log(err);
        }
      );
    });
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'bottom',
      animated:true,
      //cssClass:"my-custom-class"
    });
    toast.present();
  }

  async loadercontinue(){
    let loading = await this.loadingCtrl.create({
      message: ' Please wait. . .',
    });
  loading.present();
  }

  async hideLoader() {
    setTimeout(() => {
      this.loadingCtrl.dismiss();
    }, 2000);
  }

  async clientcompany_size() {
    //console.log("aaaaaaaa");
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + 'company-size', null).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          //console.log(err);
        }
      );
    });
  }

  // async clientcompany_type() {
  //   //console.log("aaaaaaaa");
  //   return new Promise((resolve, reject) => {
  //     this.http.get(apiUrl + 'client-registration-form', null).subscribe(res =>
  //       {
  //         resolve(res);
  //         console.log(res);
  //       },
  //       (err) =>
  //       {
  //         reject(err);
  //         //console.log(err);
  //       }
  //     );
  //   });
  // }

  async null_body_apicall(type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type, null).subscribe(res =>
        {
          resolve(res);
         // console.log(res);
        },
        (err) =>
        {
          reject(err);
        }
      );
    });
  }

  async get_method_apicall(type) {
    return new Promise((resolve, reject) => {
      this.http.get(apiUrl + type).subscribe(res =>
        {
          resolve(res);
         // console.log(res);
        },
        (err) =>
        {
          reject(err);
        }
      );
    });
  }

  async post_request_api_call(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type, credentials).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          //console.log(err);
        }
      );
    });
  }

  async can_details(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type, credentials).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          //console.log(err);
        }
      );
    });
  }

  async getCountry(){
    return new Promise(resolve => {
      this.http.get(this.countryurl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async getCountry_except_uae(){
    return new Promise(resolve => {
      this.http.get(this.exceptuaecountryurl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async getNationality(){
    return new Promise(resolve => {
      this.http.get(this.nationalityurl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async getCurrency(){
    return new Promise(resolve => {
      this.http.get(this.currencyurl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  async showalert(msg,link) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: msg,
      cssClass: 'langDelete',
      buttons: [
         {
          text: 'Ok',
          cssClass: 'secondary',
          handler: () => {
            if(link){
             this.router.navigateByUrl(link);
            }else{
              window.location.reload();
            }
             
          }
        }
      ]
    });

    await alert.present();
  }

  async Validalert(msg) {
    const alert = await this.alertController.create({
      header: 'Error!',
      message: msg,
      cssClass: 'langDelete',
      buttons: [
         {
          text: 'Ok',
          cssClass: 'secondary',
          role:"cancel"
          /* handler: () => {
            if(link){
             this.router.navigateByUrl(link);
            }else{
              window.location.reload();
            }
             
          } */
        }
      ]
    });

    await alert.present();
  }

  async Successalert(msg) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: msg,
      cssClass: 'langDelete',
      buttons: [
         {
          text: 'Ok',
          cssClass: 'secondary',
          role:"cancel"
          /* handler: () => {
            if(link){
             this.router.navigateByUrl(link);
            }else{
              window.location.reload();
            }
             
          } */
        }
      ]
    });



    await alert.present();
  }

  // async post_job(data) {
  //   console.log(data);
  //   return new Promise((resolve, reject) => {
  //     this.http.post(apiUrl+'job-posting', data).subscribe(res =>
  //       {
  //         resolve(res);
  //         console.log(res);
  //       },
  //       (err) =>
  //       {
  //         reject(err);
  //         //console.log(err);
  //       }
  //     );
  //   });
  // }

}
