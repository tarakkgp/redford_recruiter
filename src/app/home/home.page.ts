import { Component, OnInit,ViewChild } from '@angular/core';
import {DataserviceService} from '../Service/dataservice.service';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Router, ActivatedRoute} from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  @ViewChild('slides',{static: false}) slides : IonSlides;
  
  categorylist:any;
  joblist:any;
  status1: boolean = false;
  status2: boolean = false;
  status3: boolean =true;
  candidate_id:any='';
  job_count:any;
  member_count:any;
  resume_count:any;
  company_count:any;

  slideOpts = {
    // initialSlide: 1,
    speed: 1000,
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
  keyword;
  town;
  testlist: any;
  constructor(public loadingCtrl: LoadingController,
    public dataservice: DataserviceService,
    private storage: Storage,
    public router: Router,
    public acRouter: ActivatedRoute
  ) {
  }

  ngOnInit() {
    /* console.log(window.location.search.slice(1))
    let mail = window.location.search.slice(1)
    console.log(mail.slice(6))
    if(mail){
      let postdata = {
        "data":{
          "email":mail.slice(6),
        }
      }
      this.dataservice.post_request_api_call(postdata,'subscribed').then((result)=>{
        //console.log(result)
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.router.navigateByUrl('/home');
          this.dataservice.presentToast(resArray[2]);
        }else{
          this.router.navigateByUrl('/home');
          this.dataservice.presentToast(resArray[2]);
        }
    });
  } */
  }
  ionViewWillEnter(){
    this.dataservice.get_login_credential();
    console.log("client login...");
    this.dataservice.get_method_apicall('category-list').then((result)=>{
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      if(resArray[1]=="true"){
        this.categorylist= resArray[2];
        console.log(this.categorylist);
      }
    });
    this.storage.get("login_credential").then((val)=>{
      if(val){
        const resArray = Object.keys(val).map(i => val[i]);
        //console.log(resArray);
        if(resArray[0]=="true"){
        // console.log(val);
          this.candidate_id=resArray[2].candidate_id;
          this.getjoblist('Full time',this.candidate_id);
        }
      }else{
        this.getjoblist('Full time',this.candidate_id);
      }  
    });

    this.dataservice.get_method_apicall('home-all-type-count').then((result)=>{
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]==true){
        this.job_count=resArray[2];
        this.member_count=resArray[3];
        this.resume_count=resArray[4];
        this.company_count=resArray[5];
      }
    });

    this.dataservice.post_request_api_call("","home-testimonial").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.testlist=resArray[2];
      }
    });
  }

  ionViewWillLeave(){
    this.slides.stopAutoplay();
  }

  ionViewDidEnter() {
    this.slides.startAutoplay();
  }

  getjoblist(type,id){
    let postdata = {
      "data":{
        "job_type": type,
        "candadite_id": id
      }
     }

     console.log(postdata)
    this.dataservice.post_request_api_call(postdata,'job-list').then((result)=>{
      //console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]=="true"){
        this.joblist=resArray[2];
      }
    });
  }

  jobListing(id){
    this.router.navigate(['/job-listing', {id:id}])
  }

  parttime(){
    this.status1=false;
    this.status2=true;
    this.status3=false;
    this.getjoblist('Part time',this.candidate_id);
    //console.log(this.status);
  }

  featured(){
    this.status1=true;
    this.status2=false;
    this.status3=false;
    this.getjoblist('Temporary',this.candidate_id);
  }

  full_time(){
    this.status1=false;
    this.status2=false;
    this.status3=true;
    this.getjoblist('Full time',this.candidate_id);
  }

  job_details(id){
    this.router.navigate(['job-details',{id:id}]);
  }

  search(){
    if(this.keyword || this.town){
      this.router.navigate(['/job-search',{text:this.keyword?this.keyword:'',town:this.town?this.town:''}]);
    }else{
      this.dataservice.Validalert("Please enter keyword(skill) or place(town) or both.")
    }  
  }

}
