import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {DataserviceService} from '../Service/dataservice.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(public storage: Storage,
    public dataserv: DataserviceService,
    public router:Router) { }

  ngOnInit() {
    this.storage.remove('login_credential').then(()=>{
      this.dataserv.get_login_credential();
      this.router.navigateByUrl('/home');
    });
  }

}
