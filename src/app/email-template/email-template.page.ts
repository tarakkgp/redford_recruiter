import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {Router,ActivatedRoute} from "@angular/router";
import {AlertController} from '@ionic/angular';


@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.page.html',
  styleUrls: ['./email-template.page.scss'],
})
export class EmailTemplatePage implements OnInit {

  cmslist;
  errorMsg;

  constructor(
    public service: DataserviceService,
    public storage: Storage,
    public router:Router,
    public acroute: ActivatedRoute,
    public alertController: AlertController
  ) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.service.post_request_api_call('',"email-template-list").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.cmslist = resArray[2];
        this.errorMsg = null;

      }else{
        this.cmslist = null;
        this.errorMsg = resArray[2]
        //this.service.Successalert(resArray[2]);
      }
    }, error=>{
      this.service.Successalert('Server or network relatede issue');
    });
  }

  addTemplate(){
    this.router.navigateByUrl('/admin/add-template');
  }

  edit_cms(id){
    this.router.navigate(['/admin/add-template',{id:id}]);
  }

  async DeleteConfirm(id) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you want to delete!',
      cssClass: 'langDelete',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ok',
          handler: () => {
            this.deletetemplate(id);
          }
        }
      ]
    });
    await alert.present();
  }

  deletetemplate(id){
    let postdata = {
      "data":{
        "email_template_id":id
      }
    };
    this.service.post_request_api_call(postdata,"email-template-delete").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.Successalert(resArray[2]);
        this.ionViewWillEnter();
      }else{
        this.service.Successalert(resArray[2]);
      }
    }, error=>{
      this.service.Successalert('Server or network relatede issue');
    });
  }

}
