import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {DataserviceService} from '../Service/dataservice.service';
import {Storage} from '@ionic/storage';
import {Router,ActivatedRoute} from "@angular/router"

@Component({
  selector: 'app-whoweare',
  templateUrl: './whoweare.page.html',
  styleUrls: ['./whoweare.page.scss'],
})
export class WhowearePage implements OnInit {

  title = 'WHO WE ARE';
  content;
  status:any="Select status";
  id;

  constructor(
    public service:DataserviceService,
    public storage:Storage,
    public router:Router
  ) { }

  ngOnInit() {
    let postdata = {
      "data":{
        "user_type": this.service.login_type
      }
    }; 
    console.log(postdata);
    this.service.post_request_api_call(postdata,"who-we-are-details").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.title = resArray[2].title;
        this.content = resArray[2].content;
        this.status = resArray[2].status;
        this.id = resArray[2].who_we_are_id;
        //this.service.Successalert(resArray[2]);
      }else{
        this.title = '';
        this.content = '';
        this.status = 'Select status';
        this.id = null;
      }
    }, error=>{
      this.service.presentToast('Server or network issue');
    });
  }

  async validateform(form: NgForm){
    console.log(form);
    if (form.invalid) {
        //alert('invalid');
      return;
    }
    let postdata = {
      "data":{
        "who_we_are_id":this.id?this.id:'',
        "title": this.title,
        "content": this.content,
        "status": this.status,
        "user_id": this.service.userid
      }
    }; 
    console.log(postdata);
    this.service.post_request_api_call(postdata,"who-we-are-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.service.showalert(resArray[2], '');
        //window.location.reload();
      }
    }, error=>{
      this.service.presentToast('Server or network issue');
    });
  }

}
