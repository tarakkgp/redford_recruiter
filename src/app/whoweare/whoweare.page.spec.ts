import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhowearePage } from './whoweare.page';

describe('WhowearePage', () => {
  let component: WhowearePage;
  let fixture: ComponentFixture<WhowearePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhowearePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhowearePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
