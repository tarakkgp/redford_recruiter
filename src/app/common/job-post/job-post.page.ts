import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataserviceService } from '../../Service/dataservice.service';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-job-post',
  templateUrl: './job-post.page.html',
  styleUrls: ['./job-post.page.scss'],
})
export class JobPostPage implements OnInit {
  
  btnon:boolean;

  data=[];
  jstatus:any;
  jtitle:any;
  jobtype;
  cjt:any;
  opdate:any;
  cldate:any;
  country:any;
  state:any;
  city:any;
  jobcat:any;
  gender;
  gndr:any;
  Nationality:any;
  workexp;
  cwe:any;
  lang:any;
  sal;
  salry:any;
  pcity:any;
  cdname:any;
  cdcontactname:any;
  cdaddress:any;
  cdcountry;
  cdstate;
  cdcity;
  jobdescription:any;
  requirements:any;
  responsibilities:any;
  min_qualification:any;
  skills:any;
  vacancy:any;
  categorylist:any;
  type:any;
  saveasdraft='';
  cloneJob='';

  comID;
  countrylist:any;
  nationlist:any;
  statelist:any;
  stat:any;
  coun:any;
  imageurl:any;
  companylist:any;
  company:any;
  nanty:any;
  comp:any;
  currencylist: any;
  curren:any;
  currency:any;
  jst:any;
  jobid: any='';
  mainstatuslist: any;
  ////
  clientId;
  hLicense;
  lNumber;

  constructor(public loadingCtrl: LoadingController,
    private dataservice: DataserviceService,
    private storage: Storage,
    public router: Router,
    public acRouter: ActivatedRoute
  ) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.clientId = this.acRouter.snapshot.paramMap.get('cid');
    this.comID = this.acRouter.snapshot.paramMap.get('id');
    this.dataservice.getCountry().then((res)=>{
      //console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.countrylist=resArray[0];
      console.log(resArray);
    
    //this.getlist_ofcountry();
      if(this.comID){
        this.jobid = this.acRouter.snapshot.paramMap.get('jobid');
        if(this.jobid){
          let postdata = {
            "data":{
              "job_id": this.jobid
            }
          }
          //console.log(postdata)
          this.dataservice.post_request_api_call(postdata,'client-job-details').then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            console.log(resArray);
            //this.getlist_ofcountry();
            if(resArray[1]){
              //this.comid = resArray[2].company_id;
              
              this.cdname = resArray[2].company_name;
              this.cdcontactname = resArray[2].company_category_title;
              this.imageurl = resArray[2].company_logo;
              this.jtitle = resArray[2].job_title;
              //console.log(this.mainstatuslist, resArray[2].job_opening_status);
              let func = this.mainstatuslist.some(element => element.job_opening_status_id == Number(resArray[2].job_opening_status));
              //console.log(func)
              if(func){
                this.jstatus = resArray[2].job_opening_status;
              }else{
                // this.jstatus = 
                this.jst = "Select Job Status";
                //console.log('kk')
              }
              this.jobtype = resArray[2].job_type;
              this.opdate = resArray[2].date_opened;
              this.cldate = resArray[2].target_date;
              this.country = resArray[2].basic_info_cuntry;
              this.onChange(this.country);
              this.state = resArray[2].basic_info_state;
              this.city = resArray[2].basic_info_city;
              this.jobcat = resArray[2].job_category;
              if(resArray[2].person_gender){
                this.gender = resArray[2].person_gender;
              }
              if(resArray[2].person_nationality){
                this.Nationality = resArray[2].person_nationality;
              }
              this.workexp = resArray[2].person_work_exp;
              this.lang = resArray[2].person_language;
              if(resArray[2].currency){
                this.currency = resArray[2].currency;
              } 
              if(resArray[2].person_salary){
                this.sal= resArray[2].person_salary;
              }/* else{
                this.salry="Select Salary";
                this.sal="Select Salary";
              } */  
              this.pcity =resArray[2].person_city;
              this.jobdescription= resArray[2].job_description;
              this.requirements = resArray[2].requirements;
              this.responsibilities =resArray[2].responsibilities;
              this.min_qualification =resArray[2].min_qualification;
              this.skills = resArray[2].skill_set;
              this.vacancy = resArray[2].no_of_positions;
              this.hLicense = resArray[2].healthcare_license;
              this.lNumber = resArray[2].licenses_number;
            }
          });
        }else{
          let postdata = {
            "data":{
              "company_id":this.comID,
            }
          };
          this.dataservice.post_request_api_call(postdata,'company-details-on-job-posting-form').then((result)=>{
            const resArray = Object.keys(result).map(i => result[i]);
            //console.log(resArray);
            this.cdname = resArray[2].company_name;
            this.cdcontactname = resArray[2].category_title;
            this.imageurl = resArray[2].logo;
          });
        }  
      }else{
        this.storage.get("login_credential").then((val)=>{
          if(val){
            let postdata = {
              "data":{
                "client_id":this.dataservice.userid,
              }
            };
            this.dataservice.post_request_api_call(postdata,'client-company-list-on-job-posting-form').then((result)=>{
              const resArray = Object.keys(result).map(i => result[i]);
              console.log(resArray);
              this.companylist = resArray[2];
            });  
          }
        });  

      }
    });  
    this.dataservice.get_method_apicall('job-posting-form').then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      //console.log(resArray);
      if(resArray[1]){
        this.categorylist=resArray[2];
        //console.log(this.categorylist);
      }
    });
    this.jobopening_statuslist();

    

    this.dataservice.getNationality().then((res)=>{
      //console.log(res);
      const resArray = Object.keys(res).map(i => res[i]);
      this.nationlist=resArray[0];
      //console.log(resArray);
    });

    this.dataservice.getCurrency().then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      this.currencylist=resArray[0];
      //console.log(resArray);
    });

  }

  saveAsDraft(){
    this.saveasdraft = 'true';
    //document.getElementById('submit').click();
    //console.log('sa')
  }

  clonetheJob(){
    this.cloneJob = 'true';
    //document.getElementById('submit').click();
    //console.log('saccc')
  }
  
  async postJobs(form: NgForm){ 
  
    //console.log(form);
    if (form.invalid) {
        //alert('invalid');
        this.saveasdraft = '';
        this.cloneJob = '';
      return;
    }

    //this.dataservice.loadercontinue();
    let postdata = {
      "data":{
        "user_id":this.dataservice.userid,
        "user_type":this.clientId?'admin':'client',
        "job_id":this.jobid?this.jobid:'',
        "client_id":this.clientId?this.clientId:this.dataservice.userid,
        "company_id":this.comID,
        "job_opening_status":this.jstatus,
        "job_title":this.jtitle,
        "job_type":this.jobtype,
        "date_opened":this.opdate,
        "target_date":this.cldate,
        "basic_info_cuntry":this.country,
        "basic_info_state":this.state,
        "basic_info_city":this.city,
        "job_category":this.jobcat,
        "person_gender":this.gender,
        "person_nationality":this.Nationality,
        "person_work_exp":this.workexp,
        "person_language":this.lang,
        "person_salary":this.sal,
        "person_city":this.pcity,
        "currency":this.currency,
        /* "client_name":this.cdname,
        "client_contact_name":this.cdcontactname,
        "client_address":this.cdaddress, */
        /* "client_country":this.cdcountry,
        "client_state":this.cdstate,
        "client_city":this.cdcity, */
        "job_description":this.jobdescription,
        "requirements":this.requirements,
        "responsibilities": this.responsibilities,
        "min_qualification": this.min_qualification,
        "skill_set":this.skills,
        "no_of_positions":this.vacancy,
        "save_as_draft": this.saveasdraft,
        "clone_this_job": this.cloneJob,
        "healthcare_license": this.hLicense,
        "licenses_number": this.lNumber
      }
      };
      //console.log(postdata)
      this.dataservice.post_request_api_call(postdata,'job-posting').then((result)=>{
        //console.log(result);
        const resArray = Object.keys(result).map(i => result[i]);
        //console.log(resArray);
        if(resArray[0]){
          /* this.dataservice.presentToast('Job Successfully posted');
          this.dataservice.hideLoader(); */
          this.saveasdraft = '';
          this.cloneJob = '';
          if(this.clientId){
            this.dataservice.showalert(resArray[1], '/admin/job-listing;id='+this.comID+';cid='+this.clientId);
          }else{
            this.dataservice.showalert(resArray[1], '/client/job-listing;id='+this.comID);
          }
          form.onReset();
          this.jst="Select Job Status";
          this.jstatus="Select Job Status";
          this.cjt="Select Job Type";
          this.jobtype="Select Job Type";
          this.coun="Select Country";
          this.country ="Select Country";
          this.stat ="Select State";
          this.state="Select State";
          this.type="Select Job Category";
          this.jobcat="Select Job Category";
          this.gndr ="Select Gender";
          this.gender="Select Gender";
          this.nanty="Select Nationality";
          this.Nationality="Select Nationality";
          this.cwe="Select Work Experience";
          this.workexp="Select Work Experience";
          this.curren="Select Currency";
          this.currency="Select Currency";
          //this.salry="Select Salary";
          //this.sal="Select Salary";

        }else{
          /* this.dataservice.presentToast('Job posting unsuccessful');
          this.dataservice.hideLoader(); */
          this.dataservice.showalert('Job posting Faield', '');
        }
      }); 
    // console.log(this.jstatus, this.jtitle, this.opdate, this.cldate, this.country);
  }

  onChange(e){
    //console.log(e);
    this.stat = 'Select State';
    this.state = 'Select State';
    //console.log(this.countrylist);
    this.countrylist.forEach(element => {
      if(element.country==e){
        this.statelist = element.states;
        //console.log(this.statelist)
      }
    });
  }

  oncompanyChange(cmp_id){
    this.router.navigate(["/client/job-post",{id:cmp_id}]);
  }

  jobopening_statuslist(){
    this.dataservice.post_request_api_call(null,"job-opening-status-in-dropdown").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      //console.log(resArray);
      if(resArray[1]){
        this.mainstatuslist=resArray[2];
        //});
        //console.log(this.mainstatuslist);
      }
  }, error=>{
    this.dataservice.presentToast('Server or network issue');
  });
  }

}
