import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import {Router, ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.page.html',
  styleUrls: ['./job-listing.page.scss'],
})
export class JobListingPage implements OnInit {

  job_list_by_cat:any;
  job_list_by_loc:any;
  job_details_by_id:any;
  job_currency_type:any;
  job_currency:any;
  selectedSchedules =[];
  id;
  // mylist:any;
  constructor(public dataService:DataserviceService,
    public acRoute: ActivatedRoute,
    public router: Router
    ) { }

  ngOnInit() {
    this.id = this.acRoute.snapshot.paramMap.get('id');
    let postData = {
      "data":{
        "job_category_id": this.id?this.id:'',
        "job_location": ''
      }
    }
    // this.dataService.get_method_apicall('candidate-job-listing').then((result)=>{
    this.dataService.loadercontinue();
    this.dataService.jobsListing('candidate-job-listing', postData).then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.job_list_by_cat=resArray[2];
        this.job_list_by_loc=resArray[3];
        this.job_details_by_id=resArray[4];
        //document.getElementById(this.job_list_by_cat[0].category_id.toString()).click();

        //console.log(resArray[4],this.job_details_by_id);
      }else{
        this.dataService.presentToast(resArray[0]);
      }
      this.dataService.hideLoader();
    });
  }

  ionViewDidEnter() {

    this.job_list_by_cat.forEach(element => {
      if(this.job_list_by_cat.indexOf(element) == this.id){
        //console.log("llllllllllllll");
        document.getElementById(element.category_id.toString()).click();
        //console.log("llllllllllllll");
      }
    });
  }

getcategoryid(id){
    let postData = {
      "data":{
        "job_category_id": id,
        "job_location": ''
      }
    }
    // this.dataService.get_method_apicall('candidate-job-listing').then((result)=>{
    this.dataService.loadercontinue();
    this.dataService.jobsListing('candidate-job-listing', postData).then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        //this.job_list_by_cat=resArray[2];
        //this.job_list_by_loc=resArray[3];
        this.job_details_by_id=resArray[4];
        console.log(resArray[4],this.job_details_by_id);
      }else{
        this.dataService.presentToast(resArray[0]);
      }
      this.dataService.hideLoader();
    });
  }

  job_details(id){
    this.router.navigate(['job-details',{id:id}]);
  }
  /* toggleSelected(item) {             
    ///////////////// Radio Behaviour in Checkbox (Only Select 1 checkbox at time) //////////////////////////////     
    if (this.selectedSchedules.length > 0) {   
      //this.schedules.forEach(r => {r.selected = false;}) // Always 1 selected
      this.job_list_by_cat.filter(r => r.info.id != item.info.id).forEach(r => {
        r.selected = false;
      })
      this.selectedSchedules = [];         
    }
  
    if (!item.selected) { this.selectedSchedules.push(item); }
    else if (item.selected) {            
      let index = this.selectedSchedules.findIndex(c => c.info.id == item.info.id);
      if (index > -1) { this.selectedSchedules.splice(index, 1); }            
    }       
  } */

  // checking(ev){
  //   this.job_list_by_cat.forEach(element => {
  //     let objj=document.getElementById(element.category_id);
  //     if(objj.target.checked){} 
  //   });
  // }
  

}
