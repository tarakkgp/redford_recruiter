import { Component, OnInit, ElementRef } from '@angular/core';
import { Directive, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { DataserviceService } from '../../Service/dataservice.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.page.html',
  styleUrls: ['./job-details.page.scss'],
})
//@ViewChild('map') map: ElementRef;
export class JobDetailsPage implements OnInit {
  
  jobdetails;
  text = 'Apply Now';
  llocation = "Allahabad Bank, Midnapore, Paschim Medinipur, West Bengal, IN";
  file_name:any;
  rdrRslt:any;
  relatedjoblist:any;
  count: any="";
  share_url: string;
  status;

  constructor(
    public acRoute: ActivatedRoute,
    public service: DataserviceService,
    public storage: Storage,
    public router: Router,
    public render: Renderer2,
    private el: ElementRef
  ) { }

  ngOnInit() {
    this.storage.get("login_credential").then((val)=>{
        let postdata = {
          "data":{
            "job_id":this.acRoute.snapshot.paramMap.get('id'),
            "candidate_id":this.service.userid?this.service.userid:''
          }
        };
        console.log(postdata);
        this.service.post_request_api_call(postdata,'job-details').then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]==true){
            this.jobdetails=resArray[2];
            if(resArray[3] == 'true'){
              this.text = 'Applied';
              this.candidateStatus(this.acRoute.snapshot.paramMap.get('id'));
            }else{
              this.text = 'Apply Now'
            }
            this.relatedjoblist= resArray[4]
            this.count =this.relatedjoblist.length;
          }
        });
    });    
  }

  ionViewDidEnter(){
    this.share_url = window.location.href.toString();
    console.log(this.share_url);
  
  }

  candidateStatus(id){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{
            "job_id": id,
            "candidate_id": this.service.userid
          }
        };
        this.service.post_request_api_call(postdata, 'candidate-job-application-status-history').then((result)=>{
          //console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]){
            this.status = resArray[2];
            //this.service.Successalert(resArray[2]);
            //this.text = 'Applied';
          }else{
            this.status = null;
            //this.service.Validalert('Something wrong! Please try again.');
          }
        });
      }else{
        this.router.navigate(['/candidate/login', {redirect:id}]);
      }
    });
  }

  applyNow(id){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{
            "job_id": id,
            "candidate_id": this.service.userid
          }
        };
        console.log(postdata)
        this.service.post_request_api_call(postdata, 'candidate-job-apply').then((result)=>{
          console.log(result);
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[1]==true){
            this.service.Successalert(resArray[2]);
            this.text = 'Applied';
            this.candidateStatus(id);
          }else{
            this.service.Validalert('Something wrong! Please try again.');
          }
        });
      }else{
        this.router.navigate(['/candidate/login', {redirect:id}]);
      }
    });
  }

  jobListing(id){
    this.router.navigate(['/job-listing', {id:id}])
  }

  uploadcv(cv,ext){
    this.storage.get("login_credential").then((val)=>{
      if(val){
        let postdata = {
          "data":{"candidate_id":this.service.userid,
          "candidate_cv":cv,
          "cv_extension": ext
          }
          }; 
          console.log(postdata);
          this.service.loadercontinue();
        this.service.post_request_api_call(postdata,"candidate-cv-upload").then((result)=>{
          const resArray = Object.keys(result).map(i => result[i]);
          console.log(resArray);
          if(resArray[0]=="true"){ 
            //this.service.presentToast(resArray[1]);
            //this.file_name = resArray[2];
          }else{
            this.service.presentToast(resArray[0]);
          }
          this.service.hideLoader();
        });
      }  
    });    
  }

  uploadIMG(){
    document.getElementById('videoInput').click();
  }

  changeImg(ev) {
    let file = ev.files[0];
    this.file_name=file.name;
    let extension=this.file_name.split('.').pop();
    /* if(file.type=="application/msword" || file.type=="application/pdf" || file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"){ */
      if(file.type=="application/pdf"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result);
        this.rdrRslt = reader.result;
        // this.submitData(reader.result);
        this.uploadcv(this.rdrRslt,extension);
      }
      reader.readAsDataURL(file);
    }else{
      this.service.presentToast("Please select only .pdf extension document.")
      ev.value='';
    }  
  }

  shareFacebook(e) {
    e.preventDefault();
    var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.share_url, 'facebook-popup', 'height=350,width=600');
    if(facebookWindow.focus) { facebookWindow.focus(); }
    return false;
  }

  shareTwitter(e) {
    e.preventDefault();
    var twitterWindow = window.open('https://twitter.com/share?url=' + this.share_url, 'twitter-popup', 'height=350,width=600');
    if(twitterWindow.focus) { twitterWindow.focus(); }
    return false;
  }

  /* shareGooglePlus(e) {
    //  var _this = this;
    e.preventDefault();
    var GooglePlusWindow = window.open('https://plus.google.com/share?url=' + 'http://redford.bluehorse.co', 'googleplus-popup', 'height=350,width=600');
    if(GooglePlusWindow.focus) { GooglePlusWindow.focus(); }
    return false;
  } */

  shareLinkedIn(e) {
  //var url =  'http://redford.bluehorse.co';
  var title = 'Redford' //this.contributionDetails.Title;
  var text = document.URL;
  window.open('http://www.linkedin.com/shareArticle?mini=true&url='+ this.share_url +'&title='+title, 'sharer', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
  }

}
