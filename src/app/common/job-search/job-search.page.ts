import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {DataserviceService} from '../../Service/dataservice.service';

@Component({
  selector: 'app-job-search',
  templateUrl: './job-search.page.html',
  styleUrls: ['./job-search.page.scss'],
})
export class JobSearchPage implements OnInit {
  keyword:any;
  town:any;
  job_details_by_id:any;
  jobcount:any;
  errorMsgOff:boolean;

  constructor(public actroute: ActivatedRoute,
    public dataservice: DataserviceService,
    public router: Router) {
    this.keyword = this.actroute.snapshot.paramMap.get('text');
    this.town = this.actroute.snapshot.paramMap.get('town');
   }

  ngOnInit() {
    if(this.keyword || this.town){
      let postdata = {
        "data":{
          "keyword":this.keyword?this.keyword:'',
          "town":this.town?this.town:''
        }
      }
      console.log(postdata);
      this.dataservice.post_request_api_call(postdata,'home-page-job-search').then((result)=>{
        //console.log(result)
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          //this.router.navigateByUrl('/home');
          this.job_details_by_id=resArray[2];
          if(this.job_details_by_id.length>0){
            this.jobcount = this.job_details_by_id.length;
          }  
        }
    });
  }else{
    this.errorMsgOff = true;
  }
  }

  search(){
    if(this.keyword || this.town){
      this.router.navigate(['/job-search',{text:this.keyword?this.keyword:'',town:this.town?this.town:''}]);
    }else{
      this.errorMsgOff = true;
      this.dataservice.Validalert("Please enter keyword(skill) or place(town) or both.")
    }  
  }

  job_details(id){
    this.router.navigate(['job-details',{id:id}]);
  }

}
