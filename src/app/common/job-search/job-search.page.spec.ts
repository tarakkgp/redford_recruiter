import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobSearchPage } from './job-search.page';

describe('JobSearchPage', () => {
  let component: JobSearchPage;
  let fixture: ComponentFixture<JobSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobSearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
