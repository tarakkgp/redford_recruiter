import { Component, OnInit } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-cms-content',
  templateUrl: './cms-content.page.html',
  styleUrls: ['./cms-content.page.scss'],
})
export class CmsContentPage implements OnInit {
  param:any;
  title:any;
  content:any;

  constructor(public service: DataserviceService,
    public acroute: ActivatedRoute,
    public router: Router
    ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    if(this.acroute.snapshot.paramMap.get('q')){
      let postdata = {
        "data":{
          "slug":this.acroute.snapshot.paramMap.get('q')
        }
      };
      console.log(postdata);
      this.service.post_request_api_call(postdata,'home-cms-details').then((result)=>{
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[1]){
          this.title=resArray[2].title;
          this.content=resArray[2].content;
        }else{
          this.router.navigateByUrl('/home');
        }
      });
    }

  }

}
