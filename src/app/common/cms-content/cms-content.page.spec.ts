import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmsContentPage } from './cms-content.page';

describe('CmsContentPage', () => {
  let component: CmsContentPage;
  let fixture: ComponentFixture<CmsContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmsContentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmsContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
