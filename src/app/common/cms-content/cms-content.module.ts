import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CmsContentPage } from './cms-content.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {MdToHtmlmoduleModule} from '../../pipe/md-to-htmlmodule/md-to-htmlmodule.module';

const routes: Routes = [
  {
    path: '',
    component: CmsContentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MdToHtmlmoduleModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CmsContentPage]
})
export class CmsContentPageModule {}
