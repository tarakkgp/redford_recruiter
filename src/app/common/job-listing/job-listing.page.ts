import { Component, OnInit, ViewChild } from '@angular/core';
import { DataserviceService } from '../../Service/dataservice.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Storage} from '@ionic/storage';
import { IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.page.html',
  styleUrls: ['./job-listing.page.scss'],
})
export class JobListingPage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: true}) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll, {static: true}) virtualScroll: IonVirtualScroll;

  job_list_by_cat:any;
  job_list_by_loc:any;
  job_details_by_id=[];
  job_currency_type:any;
  job_currency:any;
  selectedSchedules =[];
  file_name:any;
  rdrRslt:any;
  candidate_id:any;

  id;
  activeCat;
  location:any='';
  count:any;
  ///////
  start:number=0;
  limit:number=10;
  totaldata;

  constructor(public dataService:DataserviceService,
    public acRoute: ActivatedRoute,
    public router: Router,
    public storage: Storage
    ) { }

  async ngOnInit() { 
    
  }

  ionViewWillEnter(){
    this.id = this.acRoute.snapshot.paramMap.get('id');
    this.activeCat = this.id;
    console.log(this.id);
    this.storage.get("login_credential").then((val)=>{
      if(val){
        this.getfirst_joblist('');
      }else{
        this.getfirst_joblist('');
      }
    }); 
  }

  getfirst_joblist(x){
    let catid;
    if(x){
      catid = x;
    }else{
      if(this.id){
        catid = this.id;
      }else{
        catid = '';
      }
    }
    let postData = {
      "data":{
        "job_category_id": catid,
        "job_location": this.location?this.location:'',
        "candidate_id":this.dataService.userid?this.dataService.userid:'',
        "start":this.start,
        "limit":this.limit
      }
    };
    console.log(postData);
    this.dataService.jobsListing('candidate-job-listing', postData).then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.job_list_by_cat=resArray[2];
        this.job_list_by_loc=resArray[3];
        this.id = resArray[5];
        resArray[4].forEach(element => {
          this.job_details_by_id.push(element);
        });
        this.virtualScroll.checkEnd();
        this.totaldata = resArray[6];
        //this.count = this.job_details_by_id.length;
        if(this.start+this.limit >= this.totaldata) {
          this.infiniteScroll.disabled = true;
        }
      }else{
        this.dataService.presentToast(resArray[0]);
      }
    });
    
  }

  ionViewDidEnter(){}

setcategory(id){
  this.activeCat = id;
  this.router.navigate(['/job-listing',{id:id}]);
}  

getcategoryid(id){
  this.start = 0;
  this.job_details_by_id = [];
  this.getfirst_joblist(id);
 /*    let postData = {
      "data":{
        "job_category_id": id,
        "job_location": this.location?this.location:'',
        "candidate_id":this.dataService.userid?this.dataService.userid:'',
        "start":this.start,
        "limit":this.limit
      }
    }
    console.log(postData);
    this.job_details_by_id = [];
    this.dataService.jobsListing('candidate-job-listing', postData).then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.count = this.job_details_by_id.length;
        this.id = resArray[5];
        resArray[4].forEach(element => {
          this.job_details_by_id.push(element);
        });
        this.virtualScroll.checkEnd();
        this.totaldata = resArray[6];
        console.log(resArray[4],this.job_details_by_id);
      }else{
        this.dataService.presentToast(resArray[0]);
      }
    }); */
  }

  getjobbylocation(loc,e){
    if(loc==this.location){
      e.target.checked=false;
      this.location='';
    }else{
      this.location=loc;
    }
    this.start = 0;
    this.job_details_by_id = [];
    this.getfirst_joblist(this.activeCat);
    /* let postData = {
      "data":{
        "job_category_id": this.activeCat,
        "job_location": this.location,
        "candidate_id":this.dataService.userid?this.dataService.userid:'',
        "start":this.start,
        "limit":this.limit
      }
    }
    this.job_details_by_id = [];
    this.dataService.jobsListing('candidate-job-listing', postData).then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[1]){
        this.count = this.job_details_by_id.length;
        this.id = resArray[5];
        resArray[4].forEach(element => {
          this.job_details_by_id.push(element);
        });
        this.virtualScroll.checkEnd();
        this.totaldata = resArray[6];
        console.log(resArray[4],this.job_details_by_id);
      }else{
        this.dataService.presentToast(resArray[0]);
      }
    }); */
  }

  loadData(event) {
    this.start = this.start+this.limit;
    setTimeout(() => {
      //this.ngOnInit()
      this.getfirst_joblist(this.activeCat);
      event.target.complete();
      if(this.start+this.limit >= this.totaldata) {
        event.target.disabled = true;
      }
    }, 500);
  }

  job_details(id){
    this.router.navigate(['job-details',{id:id}]);
  }
  
  uploadIMG(){
    document.getElementById('videoInput').click();
  }

  changeImg(ev) {
    let file = ev.files[0];
    this.file_name=file.name;
    let extension=this.file_name.split('.').pop();
    if(file.type=="application/pdf"){
      let reader = new FileReader();
      reader.onloadend = (e)=>{
        console.log(reader.result);
        this.rdrRslt = reader.result;
        this.uploadcv(this.rdrRslt,extension);
      }
      reader.readAsDataURL(file);
    }else{
      this.dataService.presentToast("Please select only .pdf extension document.")
      ev.value='';
    }  
  }

   addtoFav(jobid){
    let postData = {
      "data":{
        "job_id": jobid,
        "candidate_id": this.dataService.userid
      }
    };
    this.dataService.post_request_api_call(postData, 'candidate-favourite-job-upload').then((res)=>{
      const resArray = Object.keys(res).map(i => res[i]);
      if(resArray[1]==true){ 
        this.dataService.Successalert(resArray[2]);
        this.getcategoryid(this.activeCat);
      }else{
        this.dataService.Validalert(resArray[2]);
      }
    })
   }
  
  uploadcv(cv,ext){
    let postdata = {
      "data":{"candidate_id":this.candidate_id,
      "candidate_cv":cv,
      "cv_extension": ext
      }
      }; 
      console.log(postdata);
      this.dataService.loadercontinue();
    this.dataService.post_request_api_call(postdata,"candidate-cv-upload").then((result)=>{
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]=="true"){ 
        this.dataService.presentToast(resArray[1]);
        this.file_name = resArray[2];
      }else{
        this.dataService.presentToast(resArray[0]);
      }
      this.dataService.hideLoader();
    });
  }

}
